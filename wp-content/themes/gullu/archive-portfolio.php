<?php
/**
 * Template Name: Portfolio archive
 */

get_header();

$cats = get_terms('portfolio_cat');
wp_enqueue_script('mixitup');
$portfolios = new WP_Query(array(
    'post_type' => 'portfolio',
    'posts_per_page' => get_option('posts_per_page')
));
$portolio_title = function_exists('cs_get_option') ? cs_get_option('portfolio_title') : '';
$portolio_title = !empty($portolio_title) ? $portolio_title : esc_html__('Portfolio Archive', 'gullu');
?>

<div class="gullu-portfolio portfolio-full-width">
    <div class="container">
        <div class="mixitUp-menu">
            <h2> <?php echo esc_html($portolio_title); ?> </h2>
            <ul>
                <li class="filter active tran3s" data-filter="all"> <?php esc_html_e('All', 'gullu-core'); ?> </li>
                <?php
                $count = count($cats);
                if ( $count > 0 ) {
                    foreach($cats as $cat) {
                        echo  '<li class="filter tran3s" data-filter=".'.$cat->slug.'">' . $cat->name . '</li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="row" id="mixitUp-item">
        <?php
        while($portfolios->have_posts()) : $portfolios->the_post();
            $the_cats = get_the_terms(get_the_ID(), 'portfolio_cat');
            $cat_slugs = '';
            if(is_array($the_cats)) {
                foreach ($the_cats as $the_cat) {
                    $cat_slugs .= $the_cat->slug . ' ';
                }
            }
            ?>
            <div class="col-md-4 col-xs-6 mix <?php echo $cat_slugs; ?>">
                <div class="single-item">
                    <?php the_post_thumbnail('gullu_640x500') ?>
                    <div class="opacity tran3s">
                        <h5><a href="<?php the_permalink() ?>" class="tran3s"> <?php the_title(); ?> </a></h5>
                        <?php
                        if(has_post_format('video')) {
                            $portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
                            $video_url = isset($portfolio_metaboxes['video_url']) ? $portfolio_metaboxes['video_url'] : '';
                            ?>
                            <a data-fancybox href="<?php echo esc_url($video_url); ?>?rel=0&amp;showinfo=0" class="view-more tran3s"> <i class="flaticon-play-button"></i> </a>
                            <?php
                        }else { ?>
                            <a href="<?php the_permalink();; ?>" class="view-more tran3s"> <i class="flaticon-plus"></i> </a> <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php
        endwhile;
        wp_reset_postdata();
        ?>
    </div>
</div>

<?php
get_footer();