<?php

get_header();

$portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
$portfolio_atts = isset($portfolio_metaboxes['portfolio_atts']) ? $portfolio_metaboxes['portfolio_atts'] : '';
$video_url = isset($portfolio_metaboxes['video_url']) ? $portfolio_metaboxes['video_url'] : '';
?>

<div class="portfolio-details">
    <div class="container">
        <div class="title">
            <h2> <?php the_title(); ?> </h2>
            <?php gullu_social_share(); ?>
        </div> <!-- /.title -->

        <?php 
        if(has_post_format('video')) { ?>
            <div class="embed-video"> <iframe src="<?php echo esc_url($video_url); ?>" frameborder="0" allowfullscreen></iframe> </div>
            <?php
        }else{
            the_post_thumbnail('full');
        }
        ?>
        <div class="project-details-wrapper">
            <div class="row">
                <div class="col-sm-4">
                    <div class="project-info-list">
                        <ul>
                            <?php
                            if(is_array($portfolio_atts)) {
                                foreach($portfolio_atts as $portfolio_att) {
                                    ?>
                                    <li>
                                        <h6> <?php echo esc_html($portfolio_att['title']); ?> </h6>
                                        <p> <?php echo esc_html($portfolio_att['value']); ?> </p>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div> <!-- /.project-info-list -->
                </div>
                <div class="col-sm-8">
                    <div class="text">
                        <?php
                        while ( have_posts() ) : the_post();
                            the_content();
                        endwhile;
                        ?>
                    </div>
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.project-details-wrapper -->
        <ul class="page-changer clearfix">
            <?php if(get_previous_post()) { ?>
            <li class="float-left"><a href="#" class="tran3s"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> <?php previous_post_link('%link' , esc_html__('Previous', 'gullu')); ?> </a></li>
            <?php
            } elseif(get_next_post()) { ?>
            <li class="float-right"><a href="#" class="tran3s"> <?php next_post_link('%link ', esc_html__('Next', 'gullu')); ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
            <?php } ?>
        </ul> <!-- /.page-changer -->
    </div> <!-- /.container -->
</div>

<?php
get_footer();