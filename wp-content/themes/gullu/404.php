<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header();
?>

<div class="container">
    <div class="row">
        <div class="col-md-8 text-center error-text">
            <h1><?php echo esc_html__('404 ERROR', 'gullu'); ?></h1>
            <h4><?php echo esc_html__('The page you were looking for does not exist.', 'gullu'); ?></h4>
            <a href="<?php echo esc_url(home_url('/')); ?>"> <button type="button" class="btn btn-style"> <?php echo esc_html__('Go back our HomePage', 'gullu'); ?> </button> </a>
        </div>
        <br/><br/>
        <div class="col-md-4">
            <?php get_sidebar(); ?>
        </div>
    </div>
    <!-- pagination -->
</div>

<?php-
get_footer();
