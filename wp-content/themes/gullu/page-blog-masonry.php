<?php
/**
 * Template Name: Blog Masonry
 */

get_header();

global $wp_query;
global $paged;
if(is_front_page()) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
}else {
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
$temp = $wp_query;
$wp_query = null;

$wp_query = new WP_Query(array(
    'post_type'     => 'post',
    'posts_per_page'=> get_option('posts_per_page'),
    'paged'         => $paged,
));

?>

    <div class="our-blog blog-v1">
        <div class="container">
            <div class="blog-masonary row">
                <div class="grid-sizer"></div>
                <?php
                while($wp_query->have_posts()) : $wp_query->the_post();
                    ?>
                    <div class="grid-item">
                        <div class="single-blog">
                            <div class="image">
                                <?php the_post_thumbnail('gullu_370x9999'); ?>
                            </div>
                            <div class="text">
                                <h6> <?php echo get_the_author_meta('display_name'); ?> </h6>
                                <h5><a href="<?php the_permalink(); ?>" class="tran3s color-one"> <?php the_title(); ?> </a></h5>
                                <a href="<?php the_permalink(); ?>" class="tran3s"><i class="flaticon-arrows" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                ?>
            </div> <!-- /.blog-masonary -->

            <?php
            gullu_pagination();
            wp_reset_postdata();
            ?>
        </div>
    </div>

<?php
get_footer();