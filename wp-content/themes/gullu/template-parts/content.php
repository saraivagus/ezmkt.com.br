<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gullu
 */

?>

<div <?php post_class('single-blog'); ?>>
    <div class="image"> <?php the_post_thumbnail('gullu_900x450'); ?> </div>
    <div class="text">
        <h6> <?php the_author_meta('display_name'); ?> </h6>
        <h5><a href="<?php the_permalink(); ?>" class="tran3s"> <?php the_title(); ?> </a></h5>
        <p> <?php echo wp_trim_words(get_the_content(), 25, ''); ?> </p>
        <a href="<?php the_permalink(); ?>" class="tran3s"><i class="flaticon-arrows" aria-hidden="true"></i></a>
    </div>
</div>
