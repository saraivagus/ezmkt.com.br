<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gullu
 */

?>

<div class="blog-main-post">
    <?php
    the_content();
    wp_link_pages(array(
        'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'gullu' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
        'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'gullu' ) . ' </span>%',
        'separator'   => '<span class="screen-reader-text">, </span>',
    ));
    ?>
</div> <!-- /.blog-main-post -->
<div class="tag-option clearfix">
    <?php the_tags('<ul class="float-left"> <li>'.esc_html__('Tags:', 'gullu').'</li> <li>', '</li>, <li>', '</li></ul>'); ?>
    <?php
    if(defined('GULLU_CORE_FILE')) {
        gullu_social_share('float-right');
    }
    ?>
</div> <!-- /.tag-option -->