<?php

$logo = function_exists('cs_get_option') ? cs_get_option('gullu_logo') : '';
$logo = !empty($logo) ? wp_get_attachment_image_src($logo, 'full') : '';

$switch_ripples = function_exists('cs_get_option') ? cs_get_option('switch_ripples') : '';
$switch_ripples = $switch_ripples==true ? 'enable-ripples': '';

$page_metaboxes = get_post_meta(get_the_ID(), 'page_metaboxes', true);
$is_titlebar = isset($page_metaboxes['titlebar_switch']) ? $page_metaboxes['titlebar_switch'] : 1;
$menu_switch = isset($page_metaboxes['menu_switch']) ? $page_metaboxes['menu_switch'] : '';
if(is_page()) {
    $white_menu = $menu_switch == 'white_menu' ? 'menu-style-two' : '';
}else {
    $white_menu = 'menu-style-two';
}

$portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
$portfolio_title = isset($portfolio_metaboxes['titlebar_title']) ? $portfolio_metaboxes['titlebar_title'] : '';

$header_layout = function_exists('cs_get_option') ? cs_get_option('header_layout') : '';
$header_layout_img = function_exists('cs_get_option') ? cs_get_option('header_layout_img') : 'header_1';
$header_position = function_exists('cs_get_option') ? cs_get_option('header_position') : '';
$header_position = $header_position=='static' ? 'static_header' : '';
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
	<script src="//code.tidio.co/ykj4cjkvkt1f6xee6xwpmt91w7jkla3v.js"></script>
</head>

<body <?php body_class(); ?>>
<div class="main-page-wrapper <?php echo esc_attr($header_position); ?>">

    <?php
    if($header_layout_img=='header_1') {
        ?>
        <header class="theme-menu-wrapper <?php echo esc_attr($white_menu); ?> full-width-menu inner-menu">
            <div class="header-wrapper">
                <?php if ($header_layout == 'boxed') echo '<div class="container">'; ?>
                <div class="clearfix">
                    <!-- Logo -->
                    <div class="logo float-left tran4s">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <?php
                            if (!empty($logo)) { ?>
                                <img src="<?php echo esc_url($logo[0]); ?>" alt="<?php bloginfo('name'); ?>">
                                <?php
                            } else {
                                echo '<h3>' . get_bloginfo('name') . '</h3>';
                            }
                            ?>
                        </a>
                    </div>

                    <!-- ============================ Theme Menu ========================= -->
                    <nav class="theme-main-menu float-right navbar" id="mega-menu-wrapper">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only"> <?php esc_html_e('Toggle navigation', 'gullu'); ?> </span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <?php
                        wp_nav_menu(array(
                            'menu' => 'main_menu',
                            'theme_location' => 'main_menu',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id' => 'navbar-collapse-1',
                            'menu_class' => 'nav',
                            'depth' => 3,
                            'walker' => new Gullu_Nav_Navwalker,
                            'fallback_cb' => 'Gullu_Nav_Navwalker::fallback'
                        ));
                        ?>
                    </nav>
                </div>
                <?php if ($header_layout == 'boxed') echo '</div>'; ?>
            </div>
        </header> <!-- /.theme-menu-wrapper -->
        <?php
    }

    if($header_layout_img=='header_2') {
        $header2_content = function_exists('cs_get_option') ? cs_get_option('header2_content') : '';
        $is_sticky_header = function_exists('cs_get_option') ? cs_get_option('is_sticky_header') : '';
        $is_sticky_header = $is_sticky_header == 1 ? 'sticky_header' : '';
        ?>
        <header class="theme-menu-wrapper <?php echo esc_attr($is_sticky_header) ?> menu-style-three <?php echo esc_attr($white_menu); ?>">
            <div class="header-wrapper">
                <div class="top-header">
                    <div class="container">
                        <div class="float-right">
                            <?php echo wp_kses_post($header2_content); ?>
                            <?php gullu_social_links(); ?>
                        </div>
                    </div> <!-- /.container -->
                </div> <!-- /.top-header -->
                <div class="<?php echo $header_layout == 'boxed' ? 'container' : 'container-fluid'; ?>">
                    <!-- Logo -->
                    <div class="logo float-left tran4s">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <?php
                            if (!empty($logo)) { ?>
                                <img src="<?php echo esc_url($logo[0]); ?>" alt="<?php bloginfo('name'); ?>">
                                <?php
                            } else {
                                echo '<h3>' . get_bloginfo('name') . '</h3>';
                            }
                            ?>
                        </a>
                    </div>

                    <!-- ============================ Theme Menu ========================= -->
                    <nav class="theme-main-menu float-right navbar" id="mega-menu-wrapper">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only"> <?php esc_html_e('Toggle navigation', 'gullu'); ?> </span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <?php
                        wp_nav_menu(array(
                            'menu' => 'main_menu',
                            'theme_location' => 'main_menu',
                            'container_class' => 'collapse navbar-collapse',
                            'container_id' => 'navbar-collapse-1',
                            'menu_class' => 'nav',
                            'depth' => 3,
                            'walker' => new Gullu_Nav_Navwalker,
                            'fallback_cb' => 'Gullu_Nav_Navwalker::fallback'
                        ));
                        ?>
                    </nav> <!-- /.theme-main-menu -->
                </div> <!-- /.clearfix -->
            </div>
        </header>
        <?php
    }

    if ($is_titlebar == 1) {
        ?>
        <div class="inner-page-banner <?php echo esc_attr($switch_ripples); ?>">
            <div class="opacity">
                <h1> <?php gullu_page_title(); ?> </h1>
                    <?php
                    if(is_singular('portfolio')) { ?>
                        <ul>
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
                            <li>/</li>
                            <li><a href="<?php echo get_post_type_archive_link( 'portfolio' ) ?>"> <?php esc_html_e('Portfolios', 'gullu'); ?> </a></li>
                            <li>/</li>
                            <li> <?php echo esc_html($portfolio_title); ?> </li>
                        </ul>
                        <?php
                    }
                    if(is_singular('product')) {
                        $product_titlebar = get_post_meta(get_the_ID(), 'product_titlebar', true);
                        $product_title = isset($product_titlebar['titlebar_title']) ? $product_titlebar['titlebar_title'] : '';
                        ?>
                        <ul>
                            <li><a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
                            <li>/</li>
                            <li><a href="<?php echo get_post_type_archive_link( 'product' ) ?>"> <?php esc_html_e('Products', 'gullu'); ?> </a></li>
                            <li>/</li>
                            <li> <?php echo esc_html($product_title); ?> </li>
                        </ul>
                        <?php
                    }
                    elseif(!is_singular('portfolio') & !is_singular('product')) {
                        Gullu_breadcrumbs();
                    }
                    ?>
            </div>
        </div>
        <?php
    }


