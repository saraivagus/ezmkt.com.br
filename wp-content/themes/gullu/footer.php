<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gullu
 */

$copyright_text = function_exists('cs_get_option') ? cs_get_option('copyright_text') : esc_html__('© 2017 CreativeGigs. All rights reserved', 'gullu');
$footer_counter = function_exists('cs_get_option') ? cs_get_option('footer_counter') : '';
?>

<footer class="bg-two">
    <div class="container">
        <div class="row">
            <?php
            if ( is_active_sidebar( 'footer_widgets' ) ) {
                dynamic_sidebar('footer_widgets');
            }
            ?>
        </div> <!-- /.row -->

        <div class="bottom-footer clearfix">
            <ul class="float-right">
                <?php
                if(is_array($footer_counter)) {
                    foreach ($footer_counter as $count) {
                        echo '<li><h3><span class="timer p-color" data-from="' . esc_attr($count['count_from']) . '" data-to="' . esc_attr($count['count_to']) . '" data-speed="5000" data-refresh-interval="50">0</span> ' . esc_html($count['count_name']) . ' </h3></li>';
                    }
                }
                ?>
            </ul>
            <p class="float-left"> <?php echo wp_kses_post($copyright_text); ?> </p>
        </div>
    </div> <!-- /.container -->
</footer>

<!-- Sign-Up Modal -->
<div class="modal fade signUpModal theme-modal-box" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <h3> <?php esc_html_e('Login with', 'gullu'); ?> </h3>
                <?php Gullu_login_form(); ?>
            </div> <!-- /.modal-body -->
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.signUpModal -->


<!-- Scroll Top Button -->
<button class="scroll-top tran3s">
    <i class="fa fa-angle-up" aria-hidden="true"></i>
</button>


</div> <!-- /.main-page-wrapper -->

<?php wp_footer(); ?>
</body>
</html>