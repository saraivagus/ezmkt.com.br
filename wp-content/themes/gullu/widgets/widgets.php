<?php

// Register Widget areas
add_action('widgets_init', function() {

    register_sidebar(array(
        'name'          => esc_html__('Sidebar widgets', 'gullu'),
        'description'   => esc_html__('Place widgets in sidebar widgets area.', 'gullu'),
        'id'            => 'sidebar_widgets',
        'before_widget' => '<div id="%1$s" class="widgets shop_widget sidebar_widgets %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

    register_sidebar(array(
        'name'          => esc_html__('Footer widgets', 'gullu'),
        'description'   => esc_html__('Add widgets here for Footer widgets area', 'gullu'),
        'id'            => 'footer_widgets',
        'before_widget' => '<div id="%1$s" class="col-md-3 col-sm-6 col-xs-12 widgets footer-widgets %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    ));
    
    register_sidebar(array(
        'name'          => esc_html__('Shop page widgets', 'gullu'),
        'description'   => esc_html__('Add your widgets here for Shop page', 'gullu'),
        'id'            => 'gullu_shop',
        'before_widget' => '<div id="%1$s" class="widgets shop_widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

});


// Register Widgets
add_action('widgets_init', function() {
    register_widget('Gullu_About_Us');
    register_widget('Gullu_Subscribe');
});


// Require widget files
require get_template_directory() . '/widgets/about_us.php';
require get_template_directory() . '/widgets/subscribe.php';