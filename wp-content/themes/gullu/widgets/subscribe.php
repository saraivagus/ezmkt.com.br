<?php
// About us
class Gullu_Subscribe extends WP_Widget {
    public function __construct()  { // 'About us' Widget Defined
        parent::__construct('subscribe', esc_html__('(Theme) MailChimp Subscribe', 'gullu'), array(
            'description'   => esc_html__('MailChimp subscribe form.', 'gullu'),
            'classname'     => 'address-inner'
        ));
    }

    // Front End
    public function widget($args, $instance) {
        $title      = isset($instance['title']) ? $instance['title'] : '';
        $action_url = isset($instance['action_url']) ? $instance['action_url'] : '';
        echo $args['before_widget'];
        echo $args['before_title'].esc_html($title).$args['after_title'];
        ?>
        <form action="<?php echo esc_url($action_url); ?>">
            <input type="text" placeholder="<?php esc_attr_e('Enter your mail', 'gullu'); ?>">
        </form>
        <?php gullu_social_links("footer-social"); ?>
        <?php
        echo $args['after_widget'];
    }

    // Backend
    public function form($instance) {
        $title      = isset($instance['title']) ? $instance['title'] : '';
        $action_url = isset($instance['action_url']) ? $instance['action_url'] : '';
        ?>
        <table style="width:100%">
            <!-- Title -->
            <tr> <th style="text-align: left"> <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title', 'gullu') ?></label> </th> </tr>
            <tr> <td> <input type="text" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                             class="widefat" value="<?php echo esc_attr($title); ?>" placeholder="<?php esc_html_e('Enter the widget title', 'gullu'); ?>"> </td> </tr>

            <!-- Mobile number -->
            <tr> <th style="text-align: left"> <label for="<?php echo esc_attr($this->get_field_id('action_url')); ?>"><?php esc_html_e('MailChimp From Action URL', 'gullu') ?></label> </th> </tr>
            <tr> <td> <input type="text" name="<?php echo esc_attr($this->get_field_name('action_url')); ?>" id="<?php echo esc_attr($this->get_field_id('action_url')); ?>"
                             class="widefat" value="<?php echo esc_attr($action_url); ?>" placeholder="<?php esc_html_e('Enter MailChimp URL.', 'gullu'); ?>">
                    <br/>
                    <small> <?php echo wp_kses_post(__('Please follow <a href="https://goo.gl/MFB6FD" target="_blank">this guide</a> to find your Mailchimp form action URL', 'gullu')); ?> </small>
                </td> </tr>

        </table>
    <?php
    }

    // Update Data
    public function update($new_instance, $old_instance){
        $instance = $old_instance;
        $instance['title']      = $new_instance['title'];
        $instance['action_url'] = $new_instance['action_url'];

        return $instance;
    }

}