<?php
// About us
class Gullu_About_Us extends WP_Widget {
    public function __construct()  { // 'About us' Widget Defined
        parent::__construct('about_us', esc_html__('(Theme) About us', 'gullu'), array(
            'description'   => esc_html__('About us widget by Gullu', 'gullu'),
            'classname'     => 'address-inner'
        ));
    }

    // Front End
    public function widget($args, $instance) {
        $logo = function_exists('cs_get_option') ? cs_get_option('gullu_logo') : '';
        $logo = !empty($logo) ? wp_get_attachment_image_src($logo, 'full') : '';
        $logo = isset($logo[0]) ? $logo[0] : '';
        $contact_no = isset($instance['contact_no']) ? $instance['contact_no'] : '';
        $email      = isset($instance['email']) ? $instance['email'] : '';
        echo $args['before_widget'];
        ?>
        <div class="footer-logo">
            <a href="<?php esc_url(home_url('/')); ?>"><img src="<?php echo esc_url($logo); ?>" alt="Logo"></a>
            <h5><a href="mailto:<?php echo sanitize_email($email); ?>" class="tran3s"> <?php echo sanitize_email($email); ?> </a></h5>
            <h6 class="p-color"> <?php echo esc_html($contact_no); ?> </h6>
        </div>
        <?php
        echo $args['after_widget'];
    }

    // Backend
    public function form($instance) {
        $contact_no = isset($instance['contact_no']) ? $instance['contact_no'] : '';
        $email      = isset($instance['email']) ? $instance['email'] : '';
    ?>
        <table style="width:100%">
            <!-- Email -->
            <tr> <th style="text-align: left"> <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email', 'gullu') ?></label> </th> </tr>
            <tr> <td> <input type="text" name="<?php echo esc_attr($this->get_field_name('email')); ?>" id="<?php echo esc_attr($this->get_field_id('email')); ?>" class="widefat" value="<?php echo esc_attr($email); ?>" placeholder="<?php esc_html_e('Enter the email address.', 'gullu'); ?>"> </td> </tr>

            <!-- Mobile number -->
            <tr> <th style="text-align: left"> <label for="<?php echo esc_attr($this->get_field_id('contact_no')); ?>"><?php esc_html_e('Contact number', 'gullu') ?></label> </th> </tr>
            <tr> <td> <input type="text" name="<?php echo esc_attr($this->get_field_name('contact_no')); ?>" id="<?php echo esc_attr($this->get_field_id('contact_no')); ?>" class="widefat" value="<?php echo esc_attr($contact_no); ?>" placeholder="<?php esc_html_e('Enter the contact number.', 'gullu'); ?>"> </td> </tr>

        </table>
    <?php
    }

    // Update Data
    public function update($new_instance, $old_instance){
        $instance = $old_instance;
        $instance['contact_no'] = $new_instance['contact_no'];
        $instance['email']      = $new_instance['email'];

        return $instance;
    }

}