<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gullu
 */

get_header();
?>

    <div class="our-blog blog-v3">
        <div class="container">
            <div class="col-md-9 blog-posts" id="blog-list">
                <div class="wrapper">
                    <?php
                    while(have_posts()) : the_post();
                        get_template_part( 'template-parts/content', get_post_format() );
                    endwhile;
                    ?>
                </div> <!-- /.wrapper -->
                <?php gullu_pagination(); ?>
            </div>
            <div class="col-md-3 sidebar shop-sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div> <!-- /.container -->
    </div>


<?php
get_footer();