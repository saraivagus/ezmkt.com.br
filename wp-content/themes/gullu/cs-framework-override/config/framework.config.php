<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => esc_html__('Theme Settings', 'gullu'),
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'gullu-options',
  'menu_position'   => 58,
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => esc_html__( 'Gullu Theme Settings', 'gullu' ) . ' <small>' . esc_html__( 'by CreativeGigs', 'gullu' ) . '</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();


// General settings
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_general.php';


// Header settings
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_header.php';

// Footer settings
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_footer.php';

// Color options
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_colors.php';

// Typography options
//require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_typo.php';

// Blog Options
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_blog.php';

// Contact Page Setings
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_contact.php';

// Social Links
require get_template_directory().'/cs-framework-override/config/gullu_opt/opt_social_links.php';




// ------------------------------
// backup                       -
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => esc_html__('Backup', 'gullu'),
  'icon'     => 'fa fa-shield',
  'fields'   => array(
    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'gullu')
    ),
    array(
      'type'    => 'backup',
    ),
  )
);


CSFramework::instance( $settings, $options );
