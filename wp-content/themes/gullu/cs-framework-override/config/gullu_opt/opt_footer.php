<?php
$options[]      = array(
    'name'        => 'footer_options',
    'title'       => esc_html__( 'Footer Settings', 'gullu' ),
    'icon'        => 'dashicons dashicons-arrow-down-alt',
    'fields'    => array(
        array(
            'title'     => esc_html__('Footer background color', 'gullu'),
            'id'        => 'footer_bg_color',
            'type'      => 'color_picker',
        ),
        array(
            'title'     => esc_html__('Footer font color', 'gullu'),
            'id'        => 'footer_font_color',
            'type'      => 'color_picker',
            'default'   => 'rgba(0,0,0,0.65)'
        ),
        array(
            'title'     => esc_html__('Footer widget title color', 'gullu'),
            'id'        => 'footer_widget_title_color',
            'type'      => 'color_picker',
            'default'   => '#2d2d43'
        ),
        array(
            'title'     => esc_html__('Footer background image', 'gullu'),
            'id'        => 'footer_bg_img',
            'type'      => 'image',
        ),
        array(
            'id'         => 'copyright_text',
            'type'       => 'textarea',
            'title'      => esc_html__('Copyright text: ', 'gullu'),
            'desc'       => esc_html__('HTML allowed', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
            'default'	  => '© 2017 CreativeGigs. All rights reserved'
        ),
        array(
            'id'              => 'footer_counter',
            'type'            => 'group',
            'title'           => esc_html__('Footer Fact Counter', 'gullu'),
            'button_title'    => esc_html__('Add counter', 'gullu'),
            'accordion_title' => 'count_name',
            'fields'          => array(
                array(
                    'id'        => 'count_from',
                    'type'      => 'text',
                    'title'     => esc_html__('Count from', 'gullu'),
                    'default'   => '0'
                ),
                array(
                    'id'        => 'count_to',
                    'type'      => 'text',
                    'title'     => esc_html__('Count to', 'gullu'),
                    'default'   => '8997'
                ),
                array(
                    'id'        => 'count_name',
                    'type'      => 'text',
                    'title'     => esc_html__('Count Name', 'gullu'),
                    'default'   => 'Members'
                ),
            )
        ),
    ),
);