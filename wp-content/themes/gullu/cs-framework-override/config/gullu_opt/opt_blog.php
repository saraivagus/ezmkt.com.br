<?php

$options[]      = array(
    'name'      => 'blog_settings',
    'title'     => esc_html__( 'Archive Page Settings', 'gullu' ),
    'icon'      => 'dashicons dashicons-admin-post',
    'fields'    => array(
        array(
            'title'     => esc_html__('Blog page title', 'gullu'),
            'id'        => 'blog_title',
            'type'      => 'text',
            'default'   => 'Our News',
        ),
        array(
            'title'     => esc_html__('Blog page header Background', 'gullu'),
            'id'        => 'blog_header_bg',
            'type'      => 'image',
            'desc'      => esc_html__( 'Upload here a high resolution image file for blog header area.', 'gullu' ),
            'compiler'  => true,
        ),
        array(
            'title'     => esc_html__('Portfolio archive page title', 'gullu'),
            'id'        => 'portfolio_title',
            'type'      => 'text',
            'default'   => 'Portfolio archive'
        ),
        array(
            'title'     => esc_html__('Portfolio archive page header Background', 'gullu'),
            'id'        => 'portfolio_header_bg',
            'type'      => 'image',
            'compiler'  => true,
        ),
        array(
            'title'     => esc_html__('Shop page header background', 'gullu'),
            'id'        => 'shop_header_bg',
            'type'      => 'image',
            'desc'      => esc_html__( 'Upload here a high resolution image file for Shop header area.', 'gullu' ),
            'compiler'  => true,
        ),
    )

);