<?php
$options[]      = array(
    'name'        => 'social_links',
    'title'       => esc_html__( 'Social Links', 'gullu'),
    'icon'        => 'fa fa-cog',
    // Headers Variations
    'fields' => array(
        array(
            'id'    => 'facebook',
            'type'  => 'text',
            'title' => esc_html__('Facebook', 'gullu'),
            'attributes' => array(
                'style'  => 'width: 100%;'
            ),
            'default'	 => '#'
        ),
        array(
            'id'    => 'twitter',
            'type'  => 'text',
            'title' => esc_html__('Twitter', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'googleplus',
            'type'  => 'text',
            'title' => esc_html__('Google Plus', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'lnkedin',
            'type'  => 'text',
            'title' => esc_html__('Linked In', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'dribbble',
            'type'  => 'text',
            'title' => esc_html__('Dribbble', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
            'default'	  => '#'
        ),
        array(
            'id'    => 'youtube',
            'type'  => 'text',
            'title' => esc_html__('Youtube', 'gullu'),
            'attributes' => array(
                'style'    => 'width: 100%;'
            ),
        ),
        array(
            'id'              => 'social_links',
            'type'            => 'group',
            'title'           => esc_html__('Add more', 'gullu'),
            'button_title'    => esc_html__('Add new', 'gullu'),
            'accordion_title' => esc_html__('Adding New Social Link', 'gullu'),
            'fields'          => array(
                array(
                    'id'          => 'social_icon',
                    'type'        => 'icon',
                    'title'       => esc_html__('Icon', 'gullu'),
                ),
                array(
                    'id'          => 'social_link',
                    'type'        => 'text',
                    'title'       => esc_html__('Link', 'gullu'),
                    'default'     => '#'
                ),
            )
        ),

    ),
);