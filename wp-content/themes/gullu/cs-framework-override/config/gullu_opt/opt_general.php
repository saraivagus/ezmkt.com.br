<?php
/*
 * General Settings
 */

$options[]      = array(
    'name'        => 'general',
    'title'       => esc_html__( 'General Settings', 'gullu' ),
    'icon'        => 'fa fa-star',
    'fields'      => array(
        array(
            'id'      => 'is_preloader',
            'type'    => 'switcher',
            'title'   => esc_html__( 'Pre-loader ON/OFF', 'gullu' ),
            'default' => false,
        ),
        array(
            'id'      => 'switch_ripples',
            'type'    => 'switcher',
            'title'   => esc_html__( 'Enable/Disable Ripples Effect', 'gullu' ),
            'desc'    => esc_html__( 'Enable/Disable ripples effect in the page header banner.', 'gullu' ),
            'default' => false,
        ),
        array(
            'id'        => 'titlebar_padding',
            'type'      => 'text',
            'title'     => esc_html__('Title bar padding', 'gullu'),
            'desc'      => esc_html__('Input the padding as clock wise (Top Right Bottom Left).', 'gullu'),
            'default'     => '305px 0 210px 0'
        ),
        array(
            'id'        => 'titlebar_overlay_color',
            'type'      => 'color_picker',
            'title'     => esc_html__('Title bar overlay color', 'gullu'),
            'default'     => 'rgba(0,0,10,0.55)'
        ),
        array(
            'id'        => 'page_margin_top',
            'type'      => 'text',
            'title'     => esc_html__('Page margin top', 'gullu'),
            'desc'      => esc_html__('Page margin top will appear after the page title bar.', 'gullu'),
            'default'   => '190px'
        ),
    ),
);
