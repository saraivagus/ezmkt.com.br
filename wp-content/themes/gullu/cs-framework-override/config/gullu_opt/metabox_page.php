<?php
$options[]    = array(
    'id'        => 'page_metaboxes',
    'title'     => esc_html__('Page Meta fields', 'gullu'),
    'post_type' =>  array('page'),
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(
        array(
            'name'  => 'page_settings',
            'icon'  => 'dashicons dashicons-admin-settings',
            'fields' => array(
                array(
                    'id'        => 'menu_switch',
                    'type'      => 'select',
                    'title'     => esc_html__('Select a menu style.', 'gullu'),
                    'options'   => array(
                        'white_menu' => esc_html__('White Menu', 'gullu'),
                        'black_menu' => esc_html__('Black Menu', 'gullu')
                    )
                ),
            ),
        ),
        array(
            'name'  => 'page_title_bar',
            'icon'  => 'dashicons dashicons-minus',
            'fields' => array(
                array(
                    'id'        => 'titlebar_switch',
                    'type'      => 'switcher',
                    'title'     => esc_html__('ON/OFF Title bar', 'gullu'),
                    'default'   => true
                ),
            ),
        ),
    ),
);