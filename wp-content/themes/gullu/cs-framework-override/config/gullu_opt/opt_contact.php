<?php

$options[]      = array(
    'name'        => 'contact_options',
    'title'       => esc_html__( 'Contact Page Settings', 'gullu' ),
    'icon'        => 'dashicons dashicons-email-alt',
    'fields'      => array(
        array(
            'title'     => esc_html__('To', 'gullu'),
            'desc'      => esc_html__('Email receive to', 'gullu'),
            'id'        => 'mail_to',
            'type'      => 'text',
        ),
        array(
            'title'     => esc_html__('Submit button label', 'gullu'),
            'id'        => 'btn_label',
            'type'      => 'text',
            'default'   => 'SEND MESSAGE'
        ),
        array(
            'title'     => esc_html__('Contact information', 'gullu'),
            'desc'      => esc_html__('HTML supported. Place any content whatever you like.', 'gullu'),
            'id'        => 'contact_content',
            'type'      => 'textarea',
        ),
        array(
            'title'     => esc_html__('ON/OFF Social Links', 'gullu'),
            'id'        => 'switch_contact_socials',
            'type'      => 'switcher',
            'default'   => true,
        ),
        array(
            'title'     => esc_html__('ON/OFF Map?', 'gullu'),
            'id'        => 'is_map',
            'type'      => 'switcher',
            'default'   => true
        ),
        array(
            'dependency'=> array('is_map', '==', 'true'),
            'title'     => esc_html__('Google map api KEY', 'gullu'),
            'desc'      => wp_kses_post(__("Follow <a href='https://developers.google.com/maps/documentation/javascript/get-api-key' target='_blank'> this link </a> and click on Get a key", 'gullu')),
            'id'        => 'map_api',
            'type'      => 'text',
            'default'   => 'AIzaSyAChmKw_08L-8BDmgTYPqSWVGFjjjymLnE'
        ),
        array(
            'dependency'=> array('is_map', '==', 'true'),
            'title'     => esc_html__('Latitude', 'gullu'),
            'desc'      => wp_kses_post(__("Get latitude from <a href='http://www.mapcoordinates.net/en' target='_blank'>here</a>", 'gullu')),
            'id'        => 'latitude',
            'type'      => 'text',
            'default'   => '40.925372'
        ),
        array(
            'dependency'=> array('is_map', '==', 'true'),
            'title'     => esc_html__('Longitude', 'gullu'),
            'desc'      => wp_kses_post(__("Get Longitude from <a href='http://www.mapcoordinates.net/en' target='_blank'>here</a>", 'gullu')),
            'id'        => 'longitude',
            'type'      => 'text',
            'default'   => '-74.27654'
        ),
        array(
            'dependency'=> array('is_map', '==', 'true'),
            'title'     => esc_html__('Map Zoom Label', 'gullu'),
            'id'        => 'map_zoom',
            'type'      => 'text',
            'default'   => '12'
        ),
    )

);