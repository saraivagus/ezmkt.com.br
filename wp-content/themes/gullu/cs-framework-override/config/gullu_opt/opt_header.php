<?php

$options[]      = array(
    'name'        => 'header_options',
    'title'       => esc_html__( 'Header Settings', 'gullu' ),
    'icon'        => 'dashicons dashicons-arrow-up-alt',
    'sections'    => array(
        // General options
        array(
            'name'  => 'header_general',
            'title' => esc_html__('Navbar options', 'gullu'),
            'icon'  => 'dashicons dashicons-admin-generic',
            'fields'=> array(
                array(
                    'type'      => 'subheading',
                    'content'   => esc_html__('Header navigation bar settings', 'gullu')
                ),
                array(
                    'title'     => esc_html__('Logo Upload', 'gullu'),
                    'id'        => 'gullu_logo',
                    'type'      => 'image',
                    'desc'      => esc_html__( 'Upload here a image file for your logo', 'gullu' ),
                    'compiler'  => true,
                ),
                array(
                    'id'      => 'is_logo_vertical',
                    'type'    => 'switcher',
                    'title'   => esc_html__('Vertical logo', 'gullu'),
                    'desc'    => esc_html__('Vertical logo on fixed mode header.', 'gullu'),
                    'default' => false,
                ),
                array(
                    'title'     => esc_html__('Header Background color', 'gullu'),
                    'desc'     => esc_html__('Leave this field blank to use transparent header background color', 'gullu'),
                    'id'        => 'header_bg_color',
                    'type'      => 'color_picker',
                ),
                array(
                    'title'     => esc_html__('Sticky header', 'gullu'),
                    'id'        => 'is_sticky_header',
                    'type'      => 'switcher',
                    'default'   => true,
                ),
                array(
                    'title'     => esc_html__('Sticky header background color', 'gullu'),
                    'desc'      => esc_html__('Background color while the header get fixed on top.', 'gullu'),
                    'id'        => 'sticky_header_bg_color',
                    'type'      => 'color_picker',
                    'default'   => '#00d747',
                    'dependency'  => array('is_sticky_header', '==', true)
                ),
                array(
                    'title'     => esc_html__('Sticky header font color ', 'gullu'),
                    'id'        => 'sticky_header_font_color',
                    'type'      => 'color_picker',
                    'dependency'  => array('is_sticky_header', '==', true)
                ),
            )
        ),
        // Layout Options
        array(
            'name'  => 'header_layout_opt',
            'title' => esc_html__('Layout Options', 'gullu'),
            'icon'  => 'dashicons dashicons-admin-generic',
            'fields'=> array(
                array(
                    'title'     => esc_html__('Header position', 'gullu'),
                    'desc'      => esc_html__('Header position by default', 'gullu'),
                    'id'        => 'header_position',
                    'type'      => 'select',
                    'options'   => array(
                        'absolute'  => esc_html__('Absolute', 'gullu'),
                        'static'    => esc_html__('Static', 'gullu')
                    )
                ),
                array(
                    'title'     => esc_html__('Header box layout', 'gullu'),
                    'id'        => 'header_layout',
                    'type'      => 'select',
                    'options'   => array(
                        'fluid' => esc_html__('Full Width', 'gullu'),
                        'boxed' => esc_html__('Boxed', 'gullu')
                    )
                ),
                array(
                    'title'     => esc_html__('Header layout', 'gullu'),
                    'id'        => 'header_layout_img',
                    'type'      => 'image_select',
                    'options'   => array(
                        'header_1'  => GULLU_DIR_IMG.'/layouts/header_1.png',
                        'header_2'  => GULLU_DIR_IMG.'/layouts/header_2.png',
                    ),
                    'default'   => 'header_1'
                ),
                array(
                    'dependency'=> array('header_layout_img_header_2', '==', 'true'),
                    'title'     => esc_html__('Header content', 'gullu'),
                    'desc'      => esc_html__('(HTML supported.) Put some content at the top of the header. You may input your email, phone number, address etc.', 'gullu'),
                    'id'        => 'header2_content',
                    'type'      => 'textarea',
                    'default'   => '<p>Call Us <a href="#" class="tran3s">078 282 294</a></p>',
                ),
            )
        ),
        // Layout Options
        array(
            'name'  => 'menu_btn_opt',
            'title' => esc_html__('Menu Button Options', 'gullu'),
            'icon'  => 'dashicons dashicons-admin-generic',
            'fields'=> array(
                array(
                    'type'      => 'subheading',
                    'content'   => esc_html__('Menu button options', 'gullu')
                ),
                array(
                    'title'     => esc_html__('Menu button', 'gullu'),
                    'id'        => 'menu_btn',
                    'type'      => 'select',
                    'options'   => array(
                        'login_btn' => esc_html__('Login button', 'gullu'),
                        'normal_btn'=> esc_html__('Normal button', 'gullu')
                    ),
                ),
                array(
                    'title'     => esc_html__('Button label', 'gullu'),
                    'id'        => 'menu_btn_label',
                    'type'      => 'text',
                    'default'   => 'LOGIN',
                ),
                array(
                    'dependency'=> array('menu_btn', '==', 'normal_btn'),
                    'title'     => esc_html__('Button URL', 'gullu'),
                    'id'        => 'menu_btn_url',
                    'type'      => 'text',
                    'default'   => '#',
                ),
                array(
                    'dependency'=> array('menu_btn', '==', 'normal_btn'),
                    'title'     => esc_html__('Button background color', 'gullu'),
                    'id'        => 'menu_btn_bg_color',
                    'type'      => 'color_picker',
                    'default'   => '#0dda8f',
                ),
                array(
                    'dependency'=> array('menu_btn', '==', 'normal_btn'),
                    'title'     => esc_html__('Button font color', 'gullu'),
                    'id'        => 'menu_btn_font_color',
                    'type'      => 'color_picker',
                    'default'   => '#ffffff',
                ),
            )
        ),
    )

);