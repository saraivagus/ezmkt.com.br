<?php

$options[]    = array(
    'id'        => 'product_titlebar',
    'title'     => esc_html__('Titlebar settings', 'gullu'),
    'post_type' =>  array('product'),
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(
        array(
            'name'  => 'product_title_bar',
            'icon'  => 'dashicons dashicons-minus',
            'fields' => array(
                array(
                    'id'        => 'titlebar_title',
                    'type'      => 'text',
                    'title'     => esc_html__('Title-bar title', 'gullu'),
                    'default'   => 'Product details',
                ),
                array(
                    'id'        => 'titlebar_bg',
                    'type'      => 'upload',
                    'title'     => 'Title-bar background',
                ),
            ), // end: fields
        ), // end: a section
    ),
);