<?php

$options[]    = array(
    'id'        => 'post_metaboxes',
    'title'     => esc_html__('Post Meta fields', 'gullu'),
    'post_type' =>  array('post'),
    'context'   => 'side',
    'priority'  => 'default',
    'sections'  => array(
        array(
            'name'  => 'post_settings',
            'icon'  => 'dashicons dashicons-admin-settings',
            'fields' => array(
                array(
                    'id'        => 'post_color',
                    'type'      => 'color_picker',
                    'title'     => esc_html__('Post color', 'gullu'),
                    'desc'      => esc_html__('This color will be appeared at the bottom of the post title.', 'gullu'),
                    'default'   => 'rgba(199,102,255,0.32)'
                ),
            ), // end: fields
        ), // end: a section
    ),
);