<?php
$options[]      = array(
    'name'        => 'color_settings',
    'title'       => esc_html__( 'Color options', 'gullu' ),
    'icon'        => 'dashicons dashicons-admin-appearance',
    'fields'      => array(
        array(
            'title'     => esc_html__('Accent color', 'gullu'),
            'desc'      => esc_html__( 'Change the website main color.', 'gullu' ),
            'id'        => 'accent_color',
            'type'      => 'color_picker',
            'default'   => '#00d747',
        ),
        array(
            'title'     => esc_html__('Title underline color', 'gullu'),
            'id'        => 'underline_color',
            'type'      => 'color_picker',
            'default'   => '#99efb5',
        ),
    )

);