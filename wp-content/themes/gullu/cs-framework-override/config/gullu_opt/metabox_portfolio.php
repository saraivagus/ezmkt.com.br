<?php
$options[]    = array(
    'id'        => 'portfolio_metaboxes',
    'title'     => esc_html__('Portfolio Meta fields', 'gullu'),
    'post_type' =>  array('portfolio'),
    'context'   => 'normal',
    'priority'  => 'default',
    'sections'  => array(
        array(
            'name'  => 'portfolio_title_bar',
            'icon'  => 'dashicons dashicons-minus',
            'fields' => array(
                array(
                    'id'        => 'titlebar_title',
                    'type'      => 'text',
                    'title'     => esc_html__('Title-bar title', 'gullu'),
                    'default'   => 'Portfolio Details'
                ),
                array(
                    'id'        => 'titlebar_bg',
                    'type'      => 'upload',
                    'title'     => esc_html__('Title-bar background', 'gullu'),
                ),
                array(
                    'id'              => 'portfolio_atts',
                    'type'            => 'group',
                    'title'           => esc_html__('Portfolio attributes', 'gullu'),
                    'button_title'    => esc_html__('Add Attribute', 'gullu'),
                    'accordion_title' => 'title',
                    'fields'          => array(
                        array(
                            'id'        => 'title',
                            'type'      => 'text',
                            'title'     => esc_html__('Attribute Name', 'gullu'),
                        ),
                        array(
                            'id'        => 'value',
                            'type'      => 'text',
                            'title'     => esc_html__('Attribute value', 'gullu'),
                        ),
                    )
                ),
                array(
                    'id'        => 'video_url',
                    'type'      => 'textarea',
                    'title'     => esc_html__('Video embed URL', 'gullu'),
                    'desc'      => esc_html__('Video embed URL for video portfolio fromat', 'gullu'),
                    'default'   => 'https://www.youtube.com/embed/pMxgov6_5TA',
                    'sanitize'  => 'disabled'
                ),
            ),
        ),
    ),
);