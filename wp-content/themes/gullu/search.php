<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gullu
 */

get_header();

?>

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <?php
                if ( have_posts() ) {
                    while (have_posts()) : the_post();
                        get_template_part('template-parts/content', get_post_format());
                    endwhile;
                    bdt_pagination();
                }else {
                    get_template_part( 'template-parts/content', 'none' );
                }
                ?>
            </div>

            <?php get_sidebar(); ?>

        </div>
        <!-- pagination -->
    </div>

<?php
get_footer();