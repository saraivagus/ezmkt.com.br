<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gullu
 */

get_header();
?>

    <div class="blog-details blog-v3">
        <div class="container">
            <div class="wrapper">
                <?php
                while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/single', get_post_format() );
                endwhile;
                ?>
            </div> <!-- /.wrapper -->
        </div> <!-- /.container -->

        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
            comments_template();
        endif;
        ?>
    </div>

<?php
get_footer();
