<?php

add_action('woocommerce_before_main_content', 'gullu_shop_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'gullu_shop_wrapper_end', 10);

function gullu_shop_wrapper_start() {
    $class = is_singular('product') ? 'procuct-details' : 'full-width shop-page';
    echo '<div class="'.esc_attr($class).'">';
}

function gullu_shop_wrapper_end() {
    echo '</div>';
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
