<?php

function gullu_scripts(){

    /**
     * Register Google fonts.
     *
     * @return string Google fonts URL for the theme.
     */
    function gullu_fonts_url() {

        $fonts_url = '';
        $fonts     = array();
        $subsets   = '';

        /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( "on", "Lato font: on or off", 'gullu' ) ) {
            $fonts[] = "Lato:100,300,400,900";
        }

        /* translators: If there are characters in your language that are not supported by this font, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( "on", "Poppins font: on or off", 'gullu' ) ) {
            $fonts[] = "Poppins:300,400,500,600";
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }

    wp_enqueue_style('gullu-fonts', gullu_fonts_url(), array(), null);
    wp_enqueue_style('bootstrap', GULLU_DIR_VEND.'/bootstrap/bootstrap.css');
    wp_enqueue_style('camera', GULLU_DIR_VEND.'/Camera-master/css/camera.css');
    wp_enqueue_style('gullu-bootstrap-mega-menu', GULLU_DIR_VEND.'/bootstrap-mega-menu/css/menu.css');
    wp_enqueue_style('font-awesome', GULLU_DIR_FONT.'/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('owl-carousel', GULLU_DIR_VEND.'/owl-carousel/owl.carousel.css');
    wp_enqueue_style('owl-theme', GULLU_DIR_VEND.'/owl-carousel/owl.theme.css');
    wp_enqueue_style('animate', GULLU_DIR_VEND.'/WOW-master/css/libs/animate.css');
    wp_enqueue_style('gullu-hover', GULLU_DIR_VEND.'/hover.css');
    wp_enqueue_style('gullu-flaticon', GULLU_DIR_FONT.'/icon/font/flaticon.css');
    wp_enqueue_style('fancybox', GULLU_DIR_VEND.'/fancybox/dist/jquery.fancybox.min.css');
    wp_enqueue_style('jquery-ui', GULLU_DIR_VEND.'/jquery-ui/jquery-ui.min.css');
    wp_enqueue_style('gullu-wpd-style', GULLU_DIR_CSS.'/wpd-style.css');
    wp_enqueue_style('gullu-style', GULLU_DIR_CSS.'/style.css');
    wp_enqueue_style('gullu-responsive', GULLU_DIR_CSS.'/responsive.css');
    wp_enqueue_style( 'gullu-root', get_stylesheet_uri() );

    $dynamic_css = "";

    if(is_singular('post') || is_page()) {
        $dynamic_css .= '.inner-page-banner{ background: url(' . esc_url(get_the_post_thumbnail_url()) . ') no-repeat center; background-size: cover;}'."\n";
    }

    if(is_home()) {
        $header_bg = function_exists('cs_get_option') ? cs_get_option('blog_header_bg') : '';
        $blog_banner = wp_get_attachment_image_src($header_bg, 'full');
        $dynamic_css .= ".inner-page-banner { background: url(" . esc_url($blog_banner[0]) . ") no-repeat center; }";
    }
    if(is_post_type_archive('portfolio')) {
        $portfolio_header_bg = function_exists('cs_get_option') ? cs_get_option('portfolio_header_bg') : '';
        $portfolio_header_bg = wp_get_attachment_image_src($portfolio_header_bg, 'full');
        $dynamic_css .= ".inner-page-banner { background: url(" . esc_url($portfolio_header_bg[0]) . ") no-repeat center; }";
    }
    if(class_exists('WooCommerce')) {
        if (is_shop()) {
            $shop_header_bg = function_exists('cs_get_option') ? cs_get_option('shop_header_bg') : '';
            $shop_header_bg = !empty($shop_header_bg) ? wp_get_attachment_image_src($shop_header_bg, 'full') : '';
            $shop_header_bg = !empty($shop_header_bg[0]) ? $shop_header_bg[0] : '';
            $dynamic_css .= ".inner-page-banner { background: url(" . esc_url($shop_header_bg) . ") no-repeat center; }";
        }
    }
    if(is_singular('product')) {
        $product_metaboxes = get_post_meta(get_the_ID(), 'product_metaboxes', true);
        $product_title_bg = isset($product_metaboxes['titlebar_bg']) ? $product_metaboxes['titlebar_bg'] : '';
        $dynamic_css .= ".inner-page-banner { background: url(".esc_url($product_title_bg).") no-repeat center; } ";
    }
    if(is_singular('portfolio')) {
        $portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
        $titlebar_bg = isset($portfolio_metaboxes['titlebar_bg']) ? $portfolio_metaboxes['titlebar_bg'] : '';
        $dynamic_css .= ".inner-page-banner { background: url(".esc_url($titlebar_bg).") no-repeat center; } ";
    }

    if(!function_exists('cs_get_option')) {
        $dynamic_css .= 'footer.bg-two {
                            padding-top: 70px;
                            background: #f5f5f5;
                        }';
    }

    if(function_exists('cs_get_option')) {
        $accent_color = function_exists('cs_get_option') ? cs_get_option('accent_color') : '';
        $underline_color = function_exists('cs_get_option') ? cs_get_option('underline_color') : '';
        $page_margin_top = function_exists('cs_get_option') ? cs_get_option('page_margin_top') : '';
        $titlebar_padding = function_exists('cs_get_option') ? cs_get_option('titlebar_padding') : '';
        $titlebar_overlay_color = function_exists('cs_get_option') ? cs_get_option('titlebar_overlay_color') : '';
        $sticky_header_font_color = function_exists('cs_get_option') ? cs_get_option('sticky_header_font_color') : '';
        $header_bg_color = function_exists('cs_get_option') ? cs_get_option('header_bg_color') : '';
        $header_bg_color = !empty($header_bg_color) ? "background-color: $header_bg_color;" : '';
        $header_position = function_exists('cs_get_option') ? cs_get_option('header_position') : '';
        $sticky_header_bg_color = function_exists('cs_get_option') ? cs_get_option('sticky_header_bg_color') : '';
        $menu_btn_bg_color = function_exists('cs_get_option') ? cs_get_option('menu_btn_bg_color') : '';
        $menu_btn_font_color = function_exists('cs_get_option') ? cs_get_option('menu_btn_font_color') : '';
        $footer_widget_title_color = function_exists('cs_get_option') ? cs_get_option('footer_widget_title_color') : '';
        $footer_font_color = function_exists('cs_get_option') ? cs_get_option('footer_font_color') : '';
        $footer_bg_color = function_exists('cs_get_option') ? cs_get_option('footer_bg_color') : '';
        $footer_bg_color = !empty($footer_bg_color) ? "background-color: $footer_bg_color;" : '';
        $footer_bg_img = function_exists('cs_get_option') ? cs_get_option('footer_bg_img') : '';
        $footer_bg_img = wp_get_attachment_image_src($footer_bg_img, 'full');
        $footer_bg_img = isset($footer_bg_img[0]) ? $footer_bg_img[0] : '';
        $is_logo_vertical = cs_get_option('is_logo_vertical');
        if($is_logo_vertical==1) {
            $dynamic_css .= '.theme-menu-wrapper.fixed .logo {
                                -webkit-transform:rotate(90deg);
                                transform:rotate(90deg);
                            }';
        }

        if(is_singular('product')) {
            $product_titlebar = get_post_meta(get_the_ID(), 'product_titlebar', true);
            $is_product_titlebar = isset($product_titlebar['is_titlebar']) ? $product_titlebar['is_titlebar'] : '';
            $product_title = isset($product_titlebar['titlebar_title']) ? $product_titlebar['titlebar_title'] : '';
            $titlebar_bg = isset($product_titlebar['titlebar_bg']) ? $product_titlebar['titlebar_bg'] : '';
            $dynamic_css .= $is_product_titlebar!=1 ? '.inner-page-banner {background: url('.esc_url($titlebar_bg).') no-repeat center}' : '';
        }

        $dynamic_css .= '
        header.theme-menu-wrapper.fixed #mega-menu-wrapper .nav>li>a {
            color: '.esc_attr($sticky_header_font_color).';
        }
        .home-blog-section .single-blog.color-one h5 a:before {
            background: #edceff;
        }
        .theme-title h6:before, header.theme-menu-wrapper #mega-menu-wrapper .nav>li>a:before, #mega-menu-wrapper .nav>li.dropdown-holder .sub-menu li a:before,
        .banner-one h1 span:before, .our-portfolio .wrapper .image .opacity a:before, .theme-title-two h2 span:before {
            background: '.esc_attr($underline_color).';
        }
        .home-project .single-project-wrapper h2 a:hover, .home-project .single-project-wrapper:hover .content a, .theme-title-two h2 strong,
        .woocommerce ul.product_list_widget li a:hover,
        .menu-style-three .top-header p a:hover, .menu-style-three #mega-menu-wrapper .nav>li.dropdown-holder .sub-menu li a:hover, .banner-three .watch-video a,
        .banner-three .container h1 span,.banner-three .container a.button-two:hover, #theme-main-banner.banner-three .camera_prev:before, #theme-main-banner.banner-three .camera_next:before,
        .our-portfolio .wrapper .image .opacity a:after, .p-color,.what-we-do .single-block:hover h5 a,.more-about-us .main-content .main-wrapper h4:before,.home-service-section .theme-title a:hover,
        .home-service-section ul li:hover div h5 a,.testimonial-section .main-container .item .wrapper p:before,.home-blog-section .single-blog:hover>a,
        footer .footer-logo h5 a:hover,footer ul li a:hover,footer ul.footer-social li a:hover,#theme-main-banner.banner-one .camera_prev:before, #theme-main-banner.banner-one .camera_next:before,
        .our-team-styleOne .single-team-member .image .opacity ul li a,.service-version-one .single-service:hover a, .single-service:hover .text a,
        .gullu-portfolio .single-item:hover .opacity a.view-more,.portfolio-details .page-changer li a:hover,.our-blog .single-blog:hover .text>a,
        .our-blog .single-blog.solid-color:hover .text>a,.our-blog.blog-v3 .single-blog:hover .text h5 a,.our-blog.blog-v2 .single-blog:hover .text h5 a,.blog-details .tag-option ul li a:hover,
        .blog-details .comment-area .comment-section .comment button:hover,.contact-us .contact-address>a:hover,.contact-us .contact-address ul li a:hover,
        .shop-page .shop-sidebar .widget_product_categories li a:hover,.shop-page .shop-product-wrapper .single-product:hover h6 a,.shop-page .shop-product-wrapper .single-product>a:hover,
        .shop-page .shop-sidebar .popular-product li:hover .text a,header.theme-menu-wrapper.menu-style-two #mega-menu-wrapper .nav .login-button a:hover,
        .theme-modal-box .modal-content .modal-body form ul li label:before  {
          color: '.esc_attr($accent_color).' !important;
        }
        .scroll-top,
        header.theme-menu-wrapper.menu-style-three #mega-menu-wrapper .nav .quote-button a, .banner-three .container a.button-two:after, .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        .shop-page .shop-sidebar .price-ranger .ui-widget-header, .shop-page .shop-sidebar .price-ranger .ui-slider-handle, .woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
        .p-bg-color,header.theme-menu-wrapper #mega-menu-wrapper .nav .login-button a:hover,.our-portfolio .theme-title a:hover,.home-service-section .theme-title a:hover:before,
        .pricing-plan-one .nav-tabs > li.active > a,.pricing-plan-one .nav-tabs > li.active > a:hover,.pricing-plan-one .nav-tabs > li.active > a:focus,.banner-two .watch-video a:hover,
        .our-team-styleOne .title a:hover,.our-blog.blog-v2 .load-more:hover,.bootstrap-select.btn-group .dropdown-menu li.selected a,.bootstrap-select.btn-group .dropdown-menu li:hover a,
        .shop-details .procuct-details .product-info .cart-button,.shop-details .procuct-details .product-info button.wishlist-button:hover,#mega-menu-wrapper .navbar-toggle .icon-bar {
          background: '.esc_attr($accent_color).';
        }
        .banner-three .watch-video a, .single_title.title a:hover,
        header.theme-menu-wrapper #mega-menu-wrapper .nav .login-button a:hover,.our-portfolio .theme-title a:hover,.banner-two .watch-video a:hover,.our-team-styleOne .title a:hover,
        header.theme-menu-wrapper.menu-style-two #mega-menu-wrapper .nav .login-button a:hover,.gullu-portfolio .single-item .opacity a.view-more:hover:before,.our-blog.blog-v2 .load-more:hover,
        .contact-us .contact-address ul li a:hover,.shop-page .shop-sidebar .sidebar-search input:focus,.shop-details .procuct-details .product-info .cart-button,
        .shop-details .procuct-details .product-info button.wishlist-button:hover {
          border-color: '.esc_attr($accent_color).';
        }
        .contact-us-form form input:focus,
        .contact-us-form form textarea:focus {
          border-bottom-color: '.esc_attr($accent_color).';
        }
        .inner-page-banner {
            margin-bottom: '.esc_attr($page_margin_top).';
        }
        .inner-page-banner .opacity {
            background: '.esc_attr($titlebar_overlay_color).';
            padding: '.esc_attr($titlebar_padding).';
        }
        header.theme-menu-wrapper #mega-menu-wrapper .nav>li.quote-button a {
            color: '.esc_attr($menu_btn_font_color).' !important;
            background: '.esc_attr($menu_btn_bg_color). ' !important;
            padding: 10px;
            margin-top: -10px;
        }
        header.theme-menu-wrapper {
            position: '.esc_attr($header_position).';
            padding-top: 62px;
            '.esc_attr($header_bg_color).'
        }
        .theme-menu-wrapper.fixed {
            background: '.esc_attr($sticky_header_bg_color).' !important;
        }
        footer {
            background: url(' . esc_attr($footer_bg_img) . ') no-repeat center center;
            '.esc_attr($footer_bg_color).'
            background-size: cover;
        }
        footer ul li a {
            color: '.esc_attr($footer_font_color).';
        }
        footer h4 {
            color: '.esc_attr($footer_widget_title_color).';
        }
        ';
    }

    wp_add_inline_style('gullu-root', $dynamic_css);

    // Scripts
    wp_enqueue_script( 'bootstrap', GULLU_DIR_VEND . '/bootstrap/bootstrap.min.js', array('jquery'), '3.3.5', true );
    wp_enqueue_script( 'gullu-jquery.mobile.customized', GULLU_DIR_VEND . '/Camera-master/scripts/jquery.mobile.customized.min.js', array('jquery'), '1.3', true );
    wp_enqueue_script( 'jquery-easing', GULLU_DIR_VEND . '/Camera-master/scripts/jquery.easing.1.3.js', array('jquery'), '1.3', true );
    wp_enqueue_script( 'camera', GULLU_DIR_VEND . '/Camera-master/scripts/camera.min.js', array('jquery'), '1.4.0', true );
    wp_enqueue_script( 'gullu-menu', GULLU_DIR_VEND . '/bootstrap-mega-menu/js/menu.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'wow', GULLU_DIR_VEND . '/WOW-master/dist/wow.min.js', array('jquery'), '1.1.3', true );
    wp_enqueue_script( 'owl-carousel', GULLU_DIR_VEND . '/owl-carousel/owl.carousel.min.js', array('jquery'), '3.3', true );
    wp_enqueue_script( 'jquery-appear', GULLU_DIR_VEND . '/jquery.appear.js', array('jquery'), null, true );
    wp_enqueue_script( 'jquery-countTo', GULLU_DIR_VEND . '/jquery.countTo.js', array('jquery'), null, true );
    wp_enqueue_script( 'fancybox', GULLU_DIR_VEND . '/fancybox/dist/jquery.fancybox.min.js', array('jquery'), '3.0.47', true );
    wp_enqueue_script( 'ripples', GULLU_DIR_VEND . '/jquery.ripples-master/dist/jquery.ripples.js', array('jquery'), '0.6.0', true );
    wp_enqueue_script( 'fitvids', GULLU_DIR_VEND . '/jquery.fitvids.js', array('jquery'), '1.1', true );
    if(is_page_template('page-blog-masonry.php')) {
        wp_enqueue_script('masonry', GULLU_DIR_VEND.'/masonry.pkgd.min.js', array('jquery'), '4.1.1', true);
    }
    if(is_page_template('page-contact.php')) {
        $map_api_key = function_exists('cs_get_option') ? cs_get_option('map_api') : '';
        wp_enqueue_script('gullu-map-api', 'https://maps.googleapis.com/maps/api/js?key='.esc_attr($map_api_key), array(), null, true);
        wp_enqueue_script('gmaps', GULLU_DIR_VEND.'/gmaps.min.js', array(), null, true);
        wp_enqueue_script('gullu-map-script', GULLU_DIR_JS.'/map-script.js', array('gullu-theme'), null, true);
        wp_enqueue_script('validate', GULLU_DIR_VEND.'/contact-form/validate.js', array('jquery'), null, true);
    }
    $is_preloader = function_exists('cs_get_option') ? cs_get_option('is_preloader') : '';
    if($is_preloader==1) {
        wp_enqueue_script('queryLoader3', GULLU_DIR_VEND.'/queryloader/queryLoader3.js', array('jquery'), '3.0.16', true);
    }
    wp_register_script( 'mixitup', GULLU_DIR_VEND.'/jquery.mixitup.min.js', array('jquery'), '2.1.11', true );
    wp_enqueue_script( 'gullu-theme', GULLU_DIR_JS.'/theme.js"', array('jquery'), '1.0', true );
    wp_enqueue_script( 'gullu-wp-theme-custom', GULLU_DIR_JS.'/wp-theme-custom.js', array('jquery'), '1.0', true );

    $dynamic_js = '';
    $menu_btn = function_exists('cs_get_option') ? cs_get_option('menu_btn') : 1;
    $menu_btn_label = function_exists('cs_get_option') ? cs_get_option('menu_btn_label') : esc_html__('LOGIN', 'gullu');
    $menu_btn_url = function_exists('cs_get_option') ? cs_get_option('menu_btn_url') : '#';
    if($menu_btn=='login_btn'){
        if (is_user_logged_in()){
            wp_get_current_user();
            $dynamic_js .= 'jQuery("#mega-menu-wrapper .nav>li:last-child").after(\'<li class="login-button"><a href="'.esc_url(wp_logout_url(home_url('/'))).'" class="tran3s">'.esc_html__('LOGOUT', 'gullu').'</a></li>\');'."\n";
        } else {
            $dynamic_js .= 'jQuery("#mega-menu-wrapper .nav>li:last-child").after(\'<li class="login-button"><a href="#" class="tran3s" data-toggle="modal" data-target=".signUpModal">'.esc_js($menu_btn_label).'</a></li>\');'."\n";
        }
    }
    elseif($menu_btn=='normal_btn') {
        $dynamic_js .= 'jQuery("#mega-menu-wrapper .nav>li:last-child").after(\'<li class="quote-button"><a href="'.esc_url($menu_btn_url).'" class="tran3s hvr-trim-two">'.esc_js($menu_btn_label).'</a></li>\');'."\n";
    }

    $dynamic_js .= $is_preloader == 1 ?
        'jQuery(document).ready(function(){
            window.addEventListener(\'DOMContentLoaded\', function() {
                new QueryLoader2(document.querySelector("body"), {
                    barColor: "#000",
                    backgroundColor: "#fff",
                    percentage: true,
                    barHeight: 1,
                    minimumTime: 400,
                    fadeOutTime: 1000
                })
            })
        })' : '';

    wp_add_inline_script('gullu-theme', $dynamic_js);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action( 'wp_enqueue_scripts', 'gullu_scripts');