<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package gullu
 */


// Image sizes
add_image_size('gullu_460x620', 460, 620, true);
add_image_size('gullu_549x565', 549, 565, true);
add_image_size('gullu_80x80', 80, 80, true);
add_image_size('gullu_960x580', 960, 580, true);
add_image_size('gullu_570x480', 570, 480, true);
add_image_size('gullu_640x500', 640, 500, true);
add_image_size('gullu_420x370', 420, 370, true);
add_image_size('gullu_370x9999', 370, 9999, true);
add_image_size('gullu_550x600', 550, 600, true);
add_image_size('gullu_50x50', 50, 50, true);
add_image_size('gullu_430x431', 430, 431, true);
add_image_size('gullu_900x450', 900, 450, true);


// Gullu_main_menu_fallback
function Gullu_main_menu_fallback() {
    ?>
    <div class="collapse navbar-collapse" id="navbar-collapse-1">
        <ul class="nav">
            <li class="menu-list"><a href="<?php echo esc_url( admin_url( 'nav-menus.php' ) ); ?>" class="tran3s"> <?php echo esc_attr('Add a menu', 'gullu'); ?> </a></li>
        </ul>
    </div>
    <?php
}


// Comment list
function gullu_comments($comment, $args, $depth){
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    ?>
    <div class="single-comment clearfix">
        <?php
        if(get_avatar($comment)) {
            echo get_avatar($comment, 80, null, null, array('class'=> 'float-left'));
        } else {
            echo '<img src="'.GULLU_DIR_IMG.'/avatar.png" alt="" class="float-left">';
        }
        ?>
        <div class="comment float-left">
            <h6> <?php comment_author(); ?> </h6>
            <span> <?php echo get_comment_date() ?> <?php esc_html_e('At', 'gullu'); ?> <?php the_time('g:i a'); ?> </span>
            <p> <?php comment_text(); ?> </p>
            <button class="tran3s">Reply</button>
            <?php comment_reply_link(array_merge( $args, array('reply_text'=>'<button class="tran3s">'.esc_html__('Reply', 'gullu').'</button>', 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
        </div>
    </div>
<?php
}


// Pagination
function gullu_pagination() {
    the_posts_pagination(array(
        'screen_reader_text' => ' ',
        'prev_text'          => esc_html__('Previous', 'gullu'),
        'next_text'          => esc_html__('Next', 'gullu')
    ));
}


// Page Title
function gullu_page_title() {
    if(is_home()) {
        $blog_title = function_exists('cs_get_option') ? cs_get_option('blog_title') : '';
        $blog_title = !empty($blog_title) ? $blog_title : get_bloginfo('name');
        echo esc_html($blog_title);
    }
    elseif(is_post_type_archive('portfolio')) {
        $portolio_title = function_exists('cs_get_option') ? cs_get_option('portfolio_title') : '';
        $portolio_title = !empty($portolio_title) ? $portolio_title : esc_html__('Portfolio Archive', 'gullu');
        echo $portolio_title;
    }
    elseif(is_singular('portfolio')) {
        $portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
        $titlebar_title = isset($portfolio_metaboxes['titlebar_title']) ? $portfolio_metaboxes['titlebar_title'] : '';
        echo esc_html($titlebar_title);
    }
    elseif(is_singular('product')) {
        $product_titlebar = get_post_meta(get_the_ID(), 'product_titlebar', true);
        $product_title = isset($product_titlebar['titlebar_title']) ? $product_titlebar['titlebar_title'] : '';
        echo esc_html($product_title);
    }
    elseif(class_exists('WooCommerce')) {
        if (is_shop()) {
            if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                <?php woocommerce_page_title(); ?>
            <?php endif;
        }
    }
    elseif(is_archive()) {
        the_archive_title();
    }
    else {
        the_title();
    }

}


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function gullu_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'gullu_pingback_header' );


// Day link to archive page
function bdt_day_link() {
    $archive_year   = get_the_time('Y');
    $archive_month  = get_the_time('m');
    $archive_day    = get_the_time('d');
    echo get_day_link( $archive_year, $archive_month, $archive_day);
}


// Move the comment field to top
add_filter( 'comment_form_fields', function ( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
});


// Post social share options
function gullu_social_share($class='') {
    ?>
    <ul class="<?php echo esc_attr($class); ?>">
        <li> <?php esc_html_e('Share:', 'gullu'); ?> </li>
        <li><a href="https://facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="https://twitter.com/intent/tweet?text=<?php the_permalink(); ?>" target="_blank" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="https://www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>" target="_blank" class="tran3s"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
        <li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
    </ul>
    <?php
}


// Social Links
function gullu_social_links($class="") {
    $dribbble       = function_exists('cs_get_option') ? cs_get_option('dribbble') : '';
    $facebook       = function_exists('cs_get_option') ? cs_get_option('facebook') : '';
    $twitter        = function_exists('cs_get_option') ? cs_get_option('twitter') : '';
    $youtube        = function_exists('cs_get_option') ? cs_get_option('youtube') : '';
    $lnkedin        = function_exists('cs_get_option') ? cs_get_option('lnkedin') : '';
    $googleplus     = function_exists('cs_get_option') ? cs_get_option('googleplus') : '';
    $social_links   = function_exists('cs_get_option') ? cs_get_option('social_links') : '';
    ?>
    <ul class="<?php echo esc_attr($class); ?>">
        <?php if(!empty($facebook)) { ?>
            <li><a href="<?php echo esc_url($facebook); ?>" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(!empty($twitter)) { ?>
            <li><a href="<?php echo esc_url($twitter); ?>" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(!empty($dribbble)) { ?>
            <li><a href="<?php echo esc_url($dribbble); ?>" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(!empty($youtube)) { ?>
            <li><a href="<?php echo esc_url($youtube); ?>" class="tran3s"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(!empty($lnkedin)) { ?>
            <li><a href="<?php echo esc_url($lnkedin); ?>" class="tran3s"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php if(!empty($googleplus)) { ?>
            <li><a href="<?php echo esc_url($googleplus); ?>" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        <?php } ?>
        <?php
        if(is_array($social_links)) {
            foreach($social_links as $social_link) {
                echo '<li><a href="'.esc_url($social_link['social_link']).'" class="tran3s"><i class="'.esc_attr($social_link['social_icon']).'" aria-hidden="true"></i></a></li>';
            }
        }
        ?>
    </ul>
    <?php
}


// Login form
function Gullu_login_form() {
    ?>
    <form name="loginform" id="loginform" action="<?php echo esc_url(home_url('/')); ?>wp-login.php" method="post">
        <div class="wrapper">
            <input name="log" id="name_email" type="text" placeholder="<?php esc_attr_e('Username or Email', 'gullu'); ?>">
            <input type="password" name="pwd" id="pass" placeholder="<?php esc_attr_e('Password', 'gullu'); ?>">
            <ul class="clearfix">
                <li class="float-left">
                    <input type="checkbox" id="rememberme" value="forever">
                    <label for="rememberme"><?php esc_html_e('Remember Me', 'gullu'); ?></label>
                </li>
                <li class="float-right"><a href="<?php echo esc_url(home_url('/')).'/wp-login.php?action=lostpassword'; ?>" class="p-color"> <?php esc_html_e('Lost Your Password?', 'gullu'); ?> </a></li>
            </ul>
            <button type="submit" name="wp-submit" id="wp-submit" class="p-bg-color hvr-trim-two">Login</button>
            <input type="hidden" name="redirect_to" value="<?php echo esc_url(home_url('/')); ?>"/>
        </div>
    </form>
    <?php
}

// Get the 1st category name
function Gullu_first_category() {
    $cats = get_the_terms(get_the_ID(), 'category');
    $cat  = is_array($cats) ? $cats[0]->name : '';
    echo $cat;
}
// Get the 1st category link
function Gullu_first_category_link() {
    $cats = get_the_terms(get_the_ID(), 'category');
    $cat  = is_array($cats) ? get_category_link($cats[0]->term_id) : '';
    echo $cat;
}

// Add body classes
add_filter('body_class', function($classes) {
    if(is_front_page()) {
        $classes[] = 'front-page';
    }else {
        $classes[] = 'not-front-page';
    }
    return $classes;
});