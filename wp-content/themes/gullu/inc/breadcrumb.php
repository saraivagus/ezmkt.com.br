<?php
// Breadcrumbs
function Gullu_breadcrumbs() {
    $blog_title = function_exists('cs_get_option') ? cs_get_option('blog_title') : '';
    $blog_title = !empty($blog_title) ? $blog_title : get_bloginfo('name');
    echo '<ul id="crumbs">';
    if (!is_home() & !is_author() & !is_search() & !is_tag() & !is_404() & !is_archive() ) {
        echo '<li> <a href="';
        echo esc_url(home_url('/')).'">';
        echo esc_html__('home', 'gullu');
        echo "</a></li> ";
        echo '<li>/</li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><a href="'.esc_html(get_the_permalink()).'"> </a><li> ');
            if (is_single()) {
                echo "<li>/</li>";
                echo "</li><li>";
                the_title();
                echo '</li>';
            }
        }
        elseif (is_page()) {
            echo '<li>';
            echo the_title();
            echo '</li>';
        }
    }
    elseif (is_home()) { ?>
        <li> <a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
        <li>/</li>
        <li><?php echo esc_html($blog_title); ?></li>
    <?php
    }
    elseif (is_author()) { ?>
        <li> <a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
        <li>/</li>
        <li><?php echo get_the_author_meta('display_name'); ?> </li>
        <?php
    }
    elseif (is_tag()) { ?>
        <li> <?php esc_html_e('Home', 'gullu'); ?> </li>
        <li> <?php single_tag_title(); ?> </li>
        <?php
    }
    elseif (is_404()) { ?>
        <li> <a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
        <li> <?php esc_html_e('404 error', 'gullu'); ?> </li>
        <?php
    }
    elseif (is_archive()) {
        ?>
        <li><a href="<?php echo esc_url(home_url('/')); ?>"> <?php esc_html_e('Home', 'gullu'); ?> </a></li>
        <li>/</li>
        <li> <?php the_archive_title(); ?> </li>
        <?php
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>" . esc_html__('Archive for', 'gullu'); the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>" . esc_html__('Archive for', 'gullu'); the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>" . esc_html__('Archive for', 'gullu'); the_time('Y'); echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>".esc_html__('Blog Archives', 'gullu'); echo'</li>';}
    elseif (is_search()) {
        echo '<li> <i class="pe-7s-home"></i> <a href="';
        echo esc_url(home_url('/')).'">';
        echo esc_html__('home', 'gullu');
        echo "</a></li>";
        echo"<li>" . esc_html__('Search Results', 'gullu'); echo'</li>';
    }
    echo '</ul>';
}