<?php
/**
 * Template Name: Contact Page
 */

get_header();

$btn_label = function_exists('cs_get_option') ? cs_get_option('btn_label') : '';
?>


    <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-xs-12">
                    <div class="contact-us-form">
                        <form action="#" method="post" class="form-validation" autocomplete="off">
                            <input type="email" placeholder="<?php esc_attr_e('Email Address*', 'gullu'); ?>" name="email">
                            <input type="text" placeholder="<?php esc_attr_e('Subject*', 'gullu'); ?>" name="sub">
                            <textarea placeholder="<?php esc_attr_e('Your Message*', 'gullu'); ?>" name="message"></textarea>
                            <button type="submit" name="contact_submit" class="p-bg-color hvr-trim-two"> <?php echo esc_html($btn_label); ?> </button>
                        </form>
                        <?php
                        if(isset($_POST['contact_submit'])) {
                            $to = function_exists('cs_get_option') ? cs_get_option('mail_to') : '';
                            $email = isset($_POST['email']) ? $_POST['email'] : '';
                            $sub = isset($_POST['sub']) ? $_POST['sub'] : '';
                            $message = isset($_POST['message']) ? $_POST['message'] : '';
                            $body = "<b>" . esc_html__("Email: ", "gullu") . "</b>" . $email . "<br>
                                    <b>" . esc_html__("Subject: ", "gullu") . "</b>" . $sub . "<br>
                                    <b>" . esc_html__("Message: ", "gullu") . "</b>" . $message . "<br>";
                            $headers = array("Content-Type: text/html; charset=UTF-8");
                            wp_mail($to, $sub, $body);
                            if (@wp_mail($to, $sub, $body, $headers)) {
                                echo "<br> <br>" . esc_html__("Your message has sent successfully", "gullu");
                            } else {
                                echo "<br> <br>" . esc_html__("An error occurred. Please fill the form again and submit.", "gullu");
                            }
                        }
                        ?>
                    </div> <!-- /.contact-us-form -->
                </div> <!-- /.col- -->
                <div class="col-lg-5 col-md-6 col-xs-12">
                    <div class="contact-address">
                        <?php
                        $contents = function_exists('cs_get_option') ? cs_get_option('contact_content') : '';
                        echo wp_kses_post($contents);
                        $switch_contact_socials = function_exists('cs_get_option') ? cs_get_option('switch_contact_socials') : '';
                        if($switch_contact_socials==1) {
                            gullu_social_links();
                        }
                        ?>
                    </div> <!-- /.contact-address -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->
        </div> <!-- /.conatiner -->

    </div> <!-- /.contact-us -->

<?php
$is_map     = function_exists('cs_get_option') ? cs_get_option('is_map') : '';
$latitude   = function_exists('cs_get_option') ? cs_get_option('latitude') : '';
$longitude  = function_exists('cs_get_option') ? cs_get_option('longitude') : '';
$map_zoom   = function_exists('cs_get_option') ? cs_get_option('map_zoom') : '';

if($is_map==1) { ?>
    <!-- Google Map _______________________ -->
    <div id="google-map-area">
        <div class="google-map" id="contact-google-map" data-map-lat="<?php echo esc_attr($latitude); ?>" data-map-lng="<?php echo esc_attr($longitude); ?>"
             data-map-title="<?php esc_attr_e('Find Map', 'gullu'); ?>" data-map-zoom="<?php echo esc_attr($map_zoom); ?>"></div>
    </div>
    <?php
}


get_footer();