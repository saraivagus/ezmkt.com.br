<?php

/**
 * Require plugins vendor
 */

require_once get_template_directory() . '/plugins/tgm-plugin-activation/class-tgm-plugin-activation.php';
require_once get_template_directory() . '/plugins/plugins.php';

/**
 * Include the main class.
 */

include_once get_template_directory() . '/framework/classes/class-core.php';


Pisces::$template_dir_path   = get_template_directory();
Pisces::$template_dir_url    = get_template_directory_uri();
Pisces::$stylesheet_dir_path = get_stylesheet_directory();
Pisces::$stylesheet_dir_url  = get_stylesheet_directory_uri();

/**
 * Include the autoloader.
 */
include_once Pisces::$template_dir_path . '/framework/classes/class-autoload.php';

new Pisces_Autoload();

/**
 * load functions for later usage
 */

require_once Pisces::$template_dir_path . '/framework/functions/functions.php';

new Pisces_Multilingual();

if(!function_exists('pisces_init_options')){
    function pisces_init_options(){
        Pisces::$options = Pisces_Options::get_instance();
    }
    pisces_init_options();
}

if(!function_exists('Pisces')){
    function Pisces(){
        return Pisces::get_instance();
    }
}

new Pisces_Scripts();

new Pisces_Admin();

new Pisces_WooCommerce();

Pisces_Visual_Composer::get_instance();

/**
 * Set the $content_width global.
 */
global $content_width;
if ( ! is_admin() ) {
    if ( ! isset( $content_width ) || empty( $content_width ) ) {
        $content_width = (int) Pisces()->layout->get_content_width();
    }
}

require_once Pisces::$template_dir_path . '/framework/functions/extra-functions.php';