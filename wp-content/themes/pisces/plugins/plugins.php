<?php
add_action( 'tgmpa_register', 'pisces_register_required_plugins' );

if(!function_exists('pisces_register_required_plugins')){

	function pisces_register_required_plugins() {

		$plugins = array();

		$plugins[] = array(
			'name'					=> esc_html_x('WPBakery Visual Composer', 'admin-view', 'pisces'),
			'slug'					=> 'js_composer',
			'source'				=> get_template_directory() . '/plugins/js_composer.zip',
			'required'				=> true,
			'version'				=> '5.4.2'
		);

		$plugins[] = array(
			'name'					=> esc_html_x('LA Studio Core', 'admin-view', 'pisces'),
			'slug'					=> 'lastudio-core',
			'source'				=> get_template_directory() . '/plugins/lastudio-core.zip',
			'required'				=> true,
			'version'				=> '3.0.2'
		);

		$plugins[] = array(
			'name'					=> esc_html_x('Pisces Package Demo Data', 'admin-view', 'pisces'),
			'slug'					=> 'pisces-demo-data',
			'source'				=> get_template_directory() . '/plugins/pisces-demo-data.zip',
			'required'				=> true,
			'version'				=> '1.0.0'
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('WooCommerce', 'admin-view', 'pisces'),
			'slug'     				=> 'woocommerce',
			'version'				=> '3.2.1',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'					=> esc_html_x('Slider Revolution', 'admin-view', 'pisces'),
			'slug'					=> 'revslider',
			'source'				=> get_template_directory() . '/plugins/revslider.zip',
			'required'				=> false,
			'version'				=> '5.4.6'
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('Envato Market', 'admin-view', 'pisces'),
			'slug'     				=> 'envato-market',
			'source'   				=> 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
			'required' 				=> false,
			'version' 				=> '1.0.0-RC2'
		);

		$plugins[] = array(
			'name' 					=> esc_html_x('Contact Form 7', 'admin-view', 'pisces'),
			'slug' 					=> 'contact-form-7',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('YITH WooCommerce Wishlist', 'admin-view', 'pisces'),
			'slug'     				=> 'yith-woocommerce-wishlist',
			'required' 				=> false
		);

		$plugins[] = array(
			'name'     				=> esc_html_x('YITH WooCommerce Compare', 'admin-view', 'pisces'),
			'slug'     				=> 'yith-woocommerce-compare',
			'required' 				=> false
		);

		$plugins[] = array(
			'name' 					=> esc_html_x('Easy Forms for MailChimp by YIKES', 'admin-view', 'pisces'),
			'slug' 					=> 'yikes-inc-easy-mailchimp-extender',
			'required' 				=> false
		);

		$config = array(
			'id'           				=> 'pisces',
			'default_path' 				=> '',
			'menu'         				=> 'tgmpa-install-plugins',
			'has_notices'  				=> true,
			'dismissable'  				=> true,
			'dismiss_msg'  				=> '',
			'is_automatic' 				=> false,
			'message'      				=> ''
		);

		tgmpa( $plugins, $config );

	}

}