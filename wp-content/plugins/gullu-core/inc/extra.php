<?php

// Category array
function Gullu_cat_array() {
    $cats = get_categories();
    $cat_array = array();
    $cat_array['all'] = esc_html__('All', 'gullu');
    foreach ($cats as $cat) {
        $cat_array[$cat->slug] = $cat->name;
    }
    return array_flip($cat_array);
}