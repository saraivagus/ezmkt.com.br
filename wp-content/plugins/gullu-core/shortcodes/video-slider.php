<?php
add_shortcode('gullu_video_slider', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'style' => 'style_01',
    ),$atts);
    $slider_items = vc_param_group_parse_atts($content);
    $style = $atts['style']=='style_02' ? 'banner-three' : 'banner-two';
    ?>

    <div id="theme-main-banner" class="<?php echo $style; ?>">
        <?php
        foreach($slider_items as $slider_item) {
            $btn = vc_build_link($slider_item['link']);
            $bg_image = wp_get_attachment_image_src($slider_item['bg_image'], 'full');
            ?>
            <div data-src="<?php echo $bg_image[0]; ?>">
                <div class="camera_caption">
                    <div class="container">
                        <?php
                        if($atts['style']=='style_01') { ?>
                            <h5 class="wow fadeInUp animated"> <?php echo $slider_item['title']; ?> </h5>
                            <h1 class="wow fadeInUp animated" data-wow-delay="0.2s"> <?php echo $slider_item['btm_title']; ?> </h1>
                            <?php
                        }else { ?>
                            <h1 class="wow fadeInUp animated" data-wow-delay="0.2s"> <?php echo $slider_item['btm_title']; ?> </h1>
                            <h5 class="wow fadeInUp animated"> <?php echo $slider_item['title']; ?> </h5>
                            <?php
                        }
                        ?>
                        <a href="<?php echo $btn['url']; ?>" class="tran3s hvr-trim wow fadeInUp animated p-bg-color button-one" data-wow-delay="0.3s"> <?php echo $btn['title']; ?> </a>
                        <div class="play-option wow fadeInRight animated" data-wow-delay="0.255s">
                            <div class="watch-video">
                                <h6> <?php echo $slider_item['video_title_1']; ?> </h6>
                                <h4> <?php echo $slider_item['video_title_2']; ?> </h4>
                                <a data-fancybox href="<?php echo $slider_item['video_url']; ?>" class="tran3s"><i class="fa fa-play" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="wow fadeInRight animated image-shape-two" data-wow-delay="0.39s"><div class="theme-shape-two"></div></div>
                    </div> <!-- /.container -->
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Video Slider', 'gullu-core'),
                'description'       => esc_html__('Create Gullu Video Slider', 'gullu-core'),
                'base'              => 'gullu_video_slider',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Slider Item', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'title',
                                'value' => 'We’r Awesome',
                                'heading' => esc_html__('Top Title', 'gullu-core'),
                                'description' => esc_html__('Top Title is the small title.', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textarea',
                                'param_name' => 'btm_title',
                                'value' => 'Branding &amp; consulting <br>Solution for client.',
                                'heading' => esc_html__('Bottom title', 'gullu-core'),
                                'description' => esc_html__('HTML supported. Use span tag for accent colored text.', 'gullu-core'),
                            ),
                            array(
                                'type' => 'vc_link',
                                'param_name' => 'link',
                                'value' => '#',
                                'heading' => esc_html__('Button', 'gullu-core')
                            ),
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'bg_image',
                                'heading' => esc_html__('Background Image', 'gullu-core'),
                                'value' => 'Watch'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'video_title_1',
                                'heading' => esc_html__('Video Subtitle', 'gullu-core')
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'video_title_2',
                                'heading' => esc_html__('Video title', 'gullu-core'),
                                'value' => 'INTRO VIDEO'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'video_url',
                                'heading' => esc_html__('Video URL', 'gullu-core'),
                                ''
                            ),
                        )
                    )
                ),
            )
        );

    }
});