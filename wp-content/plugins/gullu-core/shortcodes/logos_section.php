<?php
add_shortcode('gullu_logo_sec', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'title' => 'Our trusted client',
        'subtitle' => '',
        'style' => 'style_01'
    ),$atts);
    $logos = vc_param_group_parse_atts($content);
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="trusted-client">
            <div class="title">
                <h2> <?php echo $atts['title'] ?> </h2>

                <p> <?php echo $atts['subtitle'] ?> </p>
            </div>
            <div class="row">
                <?php
                foreach ($logos as $logo) {
                    $logo_image = wp_get_attachment_image_src($logo['logo_image'], 'full');
                    ?>
                    <div class="col-md-4 col-xs-6">
                        <div class="client-img">
                            <a href="<?php echo $logo['url']; ?>" title="<?php echo $logo['logo_name']; ?>">
                                <img src="<?php echo $logo_image[0]; ?>" alt="<?php echo $logo['logo_name']; ?>">
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }

    elseif($atts['style']=='style_02') {
        ?>
        <div class="partent-logo-section">
            <div id="partner-logo">
            <?php
            foreach ($logos as $logo) {
                $logo_image = wp_get_attachment_image_src($logo['logo_image'], 'full'); ?>
                <div class="item">
                    <a href="<?php echo $logo['url']; ?>" title="<?php echo $logo['logo_name']; ?>">
                        <img src="<?php echo $logo_image[0]; ?>" alt="<?php echo $logo['logo_name']; ?>">
                    </a>
                </div>
                <?php
            }
            ?>
            </div>
        </div>
        <?php
    }


    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Client Logos', 'gullu-core'),
                'description'       => esc_html__('Client logos section with title.', 'gullu-core'),
                'base'              => 'gullu_logo_sec',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'Our trusted client',
                        'holder' => 'h2',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'subtitle',
                        'heading' => esc_html__('Subtitle', 'gullu-core'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Logos', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'logo_image',
                                'heading' => esc_html__('Logo image', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'url',
                                'heading' => esc_html__('Logo URL', 'gullu-core'),
                                'value' => '#'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'logo_name',
                                'heading' => esc_html__('Logo Name', 'gullu-core'),
                                'value' => 'Company Name',
                                'admin_label' => true
                            ),
                        )
                    )
                ),
            )
        );

    }
});
