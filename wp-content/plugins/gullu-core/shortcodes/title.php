<?php
add_shortcode('gullu_title', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'style' => 'style_01',
        'btn'   => ''
    ),$atts);

    $btn    = vc_build_link($atts['btn']);
    ?>
    <?php
    if($atts['style']=='style_01') { ?>
        <div class="title single_title">
            <?php echo $content; ?>
            <a href="<?php echo $btn['url']; ?>" class="tran3s"
               target="<?php echo $btn['target']; ?>"> <?php echo $btn['title']; ?> </a>
        </div>
        <?php
    }else { ?>
        <div class="theme-title-two">
            <h2> <?php echo $content; ?> </h2>
        </div>
        <?php
    }

    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Title', 'gullu-core'),
                'description'       => esc_html__('Create title with a button.', 'gullu-core'),
                'base'              => 'gullu_title',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'content',
                        'heading' => esc_html__('Title content', 'gullu-core'),
                        'description' => esc_html__('HTML supported. You can use here any heading text. Use span tag for accent colored text.', 'gullu-core'),
                        'admin_label' => true
                    ),
                    array(
                        'type' => 'vc_link',
                        'param_name' => 'btn',
                        'heading' => esc_html__('Button', 'gullu-core')
                    ),
                ),
            )
        );

    }
});
