<?php
add_shortcode('gullu_about_us', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'image' => '',
        'bg_image' => '',
        'title' => '',
        'btn_top' => 'Learn More',
        'btn'   => '',
        'style' => 'style_01',
        'bg-color' => '#f6fbfe'
    ),$atts);

    $image = wp_get_attachment_image_src($atts['image'], 'full');
    $bg_image = wp_get_attachment_image_src($atts['bg_image'], 'full');
    $bg_image = isset($bg_image[0]) ? $bg_image[0] : '';
    $btn = vc_build_link($atts['btn']);
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="more-about-us">
            <div class="image-box">
                <img src="<?php echo $image[0] ?>" class="image-shape"/>
            </div>
            <div class="theme-shape-three" style="background: url('<?php echo $bg_image ?>')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-offset-5">
                        <div class="main-content">
                            <h2> <?php echo $atts['title']; ?> </h2>
                            <div class="main-wrapper">
                                <?php echo $content; ?>
                                <div class="button-wrapper p-bg-color">
                                    <span> <?php echo $atts['btn_top']; ?> </span>
                                    <a href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>" class="hvr-icon-wobble-horizontal"> <?php echo $btn['title']; ?>
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <!-- /.button-wrapper -->
                            </div>
                            <!-- /.main-wrapper -->
                        </div>
                        <!-- /.main-content -->
                    </div>
                    <!-- /.col- -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
    <?php
    }
    elseif($atts['style']=='style_02') { ?>
        <style>
            .more-about-us.bg-color:before {
                content: '';
                width: 42%;
                height: 100%;
                position: absolute;
                left: 0;
                top: 0;
                background: url(<?php echo $image[0]; ?>) no-repeat center;
            }
        </style>

        <div class="more-about-us bg-color" style="background-color:<?php echo $atts['bg-color']; ?>">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-offset-5">
                        <div class="main-content">
                            <h2><?php echo $atts['title']; ?></h2>
                            <div class="main-wrapper">
                                <?php echo $content; ?>
                                <div class="button-wrapper p-bg-color">
                                    <span> <?php echo $atts['btn_top']; ?> </span>
                                    <a href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>" class="hvr-icon-wobble-horizontal"> <?php echo $btn['title']; ?>
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div> <!-- /.button-wrapper -->
                            </div> <!-- /.main-wrapper -->
                        </div> <!-- /.main-content -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div>
        </div>
        <?php
    }
        ?>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('About us', 'gullu-core'),
                'description'       => esc_html__('Create About us section.', 'gullu-core'),
                'base'              => 'gullu_about_us',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'bg-color',
                        'heading' => esc_html__('Background color', 'gullu-core'),
                        'value' => '#f6fbfe',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_02'
                        )
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image',
                        'heading' => esc_html__('Image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'bg_image',
                        'heading' => esc_html__('Background image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'holder' => 'h2'
                    ),
                    array(
                        'type' => 'textarea_html',
                        'param_name' => 'content',
                        'heading' => esc_html__('Content', 'gullu-core'),
                        'description' => esc_html__('HTML allowed.', 'gullu-core'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'btn_top',
                        'heading' => esc_html__('Button top text', 'gullu-core'),
                        'value' => 'Learn More'
                    ),
                    array(
                        'type' => 'vc_link',
                        'param_name' => 'btn',
                        'heading' => esc_html__('Button', 'gullu-core')
                    ),
                ),
            )
        );

    }
});
