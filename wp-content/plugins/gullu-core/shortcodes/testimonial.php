<?php
add_shortcode('gullu_testimonial', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'image' => '',
        'image_bg' => '',
        'title' => 'Check what’s our client Say about us',
        'small_title' => 'TESTIMONIALS',
        'style' => 'style_01',
        'padding' => '',
        'margin' => ''
    ),$atts);

    $image = wp_get_attachment_image_src($atts['image'], 'full');
    $image_bg = wp_get_attachment_image_src($atts['image_bg'], 'full');
    $image_bg = isset($image_bg[0]) ? $image_bg[0] : '';
    $quotes = vc_param_group_parse_atts($content);
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="testimonial-section" style="padding: <?php echo $atts['padding'] ?>; margin: <?php echo $atts['margin'] ?>;">
            <div class="image-box wow fadeInLeft">
                <img src="<?php echo $image[0]; ?>" class="image-shape"/>
            </div>
            <div class="theme-shape-four wow fadeInLeft" style="background: url('<?php echo $image_bg ?>')"></div>
            <div class="container">
                <div class="main-container col-md-6 col-md-offset-6">
                    <div class="theme-title">
                        <h6> <?php echo $atts['small_title']; ?> </h6>
                        <h2> <?php echo $atts['title']; ?> </h2>
                    </div> <!-- /.theme-title -->
                    <div class="testimonial-slider">
                        <?php
                        foreach($quotes as $quote) {
                            $author_image = wp_get_attachment_image_src($quote['author_image'], 'gullu_50x50');
                            ?>
                            <div class="item">
                                <div class="wrapper">
                                    <p> <?php echo $quote['quote']; ?> </p>
                                    <div class="name clearfix">
                                        <img src="<?php echo $author_image[0]; ?>" alt="<?php echo $quote['author_name']; ?>">
                                        <h5> <?php echo $quote['author_name']; ?> </h5>
                                        <span> <?php echo $quote['author_designation']; ?> </span>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div> <!-- /.testimonial-slider -->
                </div> <!-- /.main-container -->
            </div> <!-- /.container -->
        </div>
        <?php
    }

    elseif($atts['style']=='style_02') {
        ?>
        <style>
            .testimonial-section.bg-image {
                background: url(<?php echo $image[0]; ?>) no-repeat center;
                background-size: cover;
            }
        </style>
        <div class="testimonial-section bg-image" style="padding: <?php echo $atts['padding'] ?>; margin: <?php echo $atts['margin'] ?>;">
            <div class="container">
                <div class="main-container col-md-6 col-md-offset-6">
                    <div class="theme-title">
                        <h6> <?php echo $atts['small_title']; ?> </h6>
                        <h2> <?php echo $atts['title']; ?> </h2>
                    </div>
                    <!-- /.theme-title -->
                    <div class="testimonial-slider">
                        <?php
                        foreach($quotes as $quote) {
                            $author_image = wp_get_attachment_image_src($quote['author_image'], 'gullu_50x50');
                            ?>
                            <div class="item">
                                <div class="wrapper">
                                    <p> <?php echo $quote['quote']; ?> </p>
                                    <div class="name clearfix">
                                        <img src="<?php echo $author_image[0]; ?>" alt="<?php echo $quote['author_name']; ?>">
                                        <h5> <?php echo $quote['author_name']; ?> </h5>
                                        <span> <?php echo $quote['author_designation']; ?> </span>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <!-- /.item -->
                    </div>
                    <!-- /.testimonial-slider -->
                </div>
                <!-- /.main-container -->
            </div>
            <!-- /.container -->
        </div>
        <?php
    }
    ?>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Testimonial', 'gullu-core'),
                'description'       => esc_html__('Full width section', 'gullu-core'),
                'base'              => 'gullu_testimonial',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'Check what’s our client Say about us'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'small_title',
                        'heading' => esc_html__('Small title', 'gullu-core'),
                        'value' => 'TESTIMONIALS',
                        'holder' => 'h2'
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image',
                        'heading' => esc_html__('Left Image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image_bg',
                        'heading' => esc_html__('Background image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Counters', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textarea',
                                'param_name' => 'quote',
                                'heading' => esc_html__('Quote Text', 'gullu-core'),
                            ),
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'author_image',
                                'heading' => esc_html__('Quote Author image', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'author_name',
                                'heading' => esc_html__('Author Name', 'gullu-core'),
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'author_designation',
                                'heading' => esc_html__('Author Designation', 'gullu-core'),
                            ),
                        )
                    ),

                    // Group: Styling
                    array(
                        'type' => 'textfield',
                        'param_name' => 'padding',
                        'heading' => 'Padding',
                        'description' => 'Padding around the section. Input the padding as clock wise (Top Right Bottom Left)',
                        'group' => 'Styling'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'margin',
                        'heading' => 'Margin',
                        'description' => 'Margin around the section. Input the padding as clock wise (Top Right Bottom Left)',
                        'group' => 'Styling'
                    ),
                ),
            )
        );

    }
});
