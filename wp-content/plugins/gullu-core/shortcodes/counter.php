<?php
add_shortcode('gullu_counter', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'border' => 'with_border',
        'style' => 'style_01',
        'padding' => '130px 0 50px 0'
    ),$atts);

    $counters = vc_param_group_parse_atts($content);
    $border = $atts['border']=='without_border' ? 'no-border fix' : '';
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="theme-counter <?php echo $border; ?>" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="row">
                <?php
                foreach ($counters as $counter) {
                    $count_append = isset($counter['count_append']) ? $counter['count_append'] : '';
                    $count_label = isset($counter['count_label']) ? $counter['count_label'] : '';
                    ?>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <div class="single-box">
                            <h2 class="number">
                                <span class="timer" data-from="<?php echo $counter['count_start']; ?>" data-to="<?php echo $counter['count_to'] ?>" data-speed="1000" data-refresh-interval="5">0</span>
                                <?php echo $count_append; ?>
                            </h2>
                            <p> <?php echo $count_label; ?> </p>
                        </div>
                        <!-- /.single-box -->
                    </div>
                    <?php
                }
                ?>
            </div>
            <!-- /.row -->
        </div>
        <?php
    }
    elseif($atts['style']=='style_02') {
        ?>
        <div class="theme-counter-styleTwo" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="container">
                <ul class="clearfix">
                    <?php
                    foreach ($counters as $counter) {
                        $count_append = isset($counter['count_append']) ? $counter['count_append'] : '';
                        $count_prepend = isset($counter['count_prepend']) ? $counter['count_prepend'] : '';
                        $count_label = isset($counter['count_label']) ? $counter['count_label'] : '';
                        ?>
                        <li class="float-left">
                            <p><?php echo $count_label; ?></p>
                            <h2 class="number"><?php echo $count_prepend; ?><span class="timer" data-from="<?php echo $counter['count_start']; ?>" data-to="<?php echo $counter['count_to'] ?>" data-speed="1000" data-refresh-interval="5">0</span><?php echo $count_append; ?></h2>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div> <!-- /.container -->
        </div>
        <?php
    }


    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Counter', 'gullu-core'),
                'description'       => esc_html__('Display counter section.', 'gullu-core'),
                'base'              => 'gullu_counter',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02 (Border boxed)' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'border',
                        'value' => array(
                            'With Border' => 'with_border',
                            'Without Border' => 'without_border'
                        ),
                        'holder' => 'h2',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Counters', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'count_start',
                                'heading' => esc_html__('Count start from', 'gullu-core'),
                                'description' => esc_html__('Input the value in numeric number', 'gullu-core'),
                                'value' => '0'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'count_to',
                                'heading' => esc_html__('Count to', 'gullu-core'),
                                'description' => esc_html__('Input the value in numeric number', 'gullu-core')
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'count_prepend',
                                'heading' => esc_html__('Count prepend text', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'count_append',
                                'heading' => esc_html__('Count append text', 'gullu-core'),
                                'value' => '+'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'count_label',
                                'heading' => esc_html__('Count label', 'gullu-core'),
                            ),
                        )
                    ),
                    // Group: Styling
                    array(
                        'type' => 'textfield',
                        'param_name' => 'padding',
                        'heading' => 'Padding',
                        'description' => 'Padding around the section. Input the padding as clock wise (Top Right Bottom Left)',
                        'value' => '130px 0 50px 0',
                        'group' => 'Styling'
                    ),
                ),
            )
        );

    }
});
