<?php
add_shortcode('gullu_pricing', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'image' => '',
        'title' => 'Not any hidden charge, Choose you pricing plan',
        'small_title' => 'Our pricing',
        'desc' => 'We have differe type of pricing table to choose with your need with resonable price.',
        'tab_items' => ''
    ),$atts);

    $image = wp_get_attachment_image_src($atts['image'], 'full');

    $tab_items = vc_param_group_parse_atts($atts['tab_items']);
    $tab_contents = vc_param_group_parse_atts($content);
    ?>

    <div class="pricing-plan-one">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 wow fadeInLeft">
                    <div class="theme-title">
                        <h6> <?php echo $atts['small_title']; ?> </h6>
                        <h2> <?php echo $atts['title']; ?> </h2>
                        <p> <?php echo $atts['desc']; ?> </p>
                    </div> <!-- /.theme-title -->
                    <ul class="nav nav-tabs">
                        <?php
                        $i = 0;
                        foreach($tab_items as $tab_item) {
                            $active = $i==0 ? 'class="active"' : '';
                            echo '<li '.$active.'><a data-toggle="tab" href="#'.$tab_item['id'].'">'.$tab_item['name'].'</a></li>';
                            $i ++;
                        }
                        ?>
                    </ul>
                </div> <!-- /.col- -->

                <div class="col-md-6 col-xs-12 wow fadeInRight">
                    <div class="tab-content">
                        <?php
                        unset($i);
                        $i = 0;
                        foreach($tab_contents as $tab_content) {
                            $active = $i == 0 ? 'active' : '';
                            $price  = !empty($tab_content['price']) ?  $tab_content['price'] : '';
                            $dot_pos= strpos($price, '.');
                            $dot    = substr($price, $dot_pos, 1);
                            $dot_pos1  = $dot_pos + 1;
                            $dot_pos_1  = $dot_pos - 1;
                            $after_dot = substr($price, $dot_pos1);
                            $before_dot = substr($price, 1, $dot_pos_1);
                            ?>
                            <div id="<?php echo $tab_content['id'] ?>" class="tab-pane fade in <?php echo $active; ?>">
                                <div class="clearfix">
                                    <div class="float-left left-side">
                                        <?php if(!empty($tab_content['price'])) : ?>
                                            <span> <sub><?php echo $price[0]; ?></sub><?php echo $before_dot.$dot; ?><sup><?php echo $after_dot; ?></sup></span>
                                        <?php endif; ?>
                                        <h6><?php echo $tab_content['name']; ?></h6>
                                        <a href="<?php echo $tab_content['url'] ?>">+</a>
                                    </div> <!-- /.left-side -->
                                    <div class="right-side float-left">
                                        <?php echo $tab_content['tab_content']; ?>
                                    </div> <!-- /.right-side -->
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Pricing Tabs', 'gullu-core'),
                'description'       => esc_html__('Create & display pricing. Full width Section', 'gullu-core'),
                'base'              => 'gullu_pricing',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'Not any hidden charge, Choose you pricing plan'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'small_title',
                        'heading' => esc_html__('Small title', 'gullu-core'),
                        'value' => 'OUR PRICING',
                        'holder' => 'h2'
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'desc',
                        'heading' => esc_html__('Description', 'gullu-core'),
                        'value' => 'We have differe type of pricing table to choose with your need with resonable price.',
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'tab_items',
                        'heading' => esc_html__('Tab items', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'id',
                                'heading' => esc_html__('Tab ID', 'gullu-core'),
                                'description' => esc_html__('ID should be an unique name (without space).', 'gullu-core'),
                                'value' => 'plan1'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'name',
                                'heading' => esc_html__('Tab Name', 'gullu-core'),
                                'value' => 'Monthly',
                                'admin_label' => true
                            ),
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Tab Contents', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'id',
                                'heading' => esc_html__('Tab ID', 'gullu-core'),
                                'description' => esc_html__('ID should be an unique name (without space).', 'gullu-core'),
                                'value' => 'plan1'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'name',
                                'heading' => esc_html__('Tab Name', 'gullu-core'),
                                'value' => 'Monthly',
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'price',
                                'heading' => esc_html__('Plan price', 'gullu-core'),
                                'value' => '$96.99'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'url',
                                'heading' => esc_html__('Plan URL', 'gullu-core'),
                                'value' => '#'
                            ),
                            array(
                                'type' => 'textarea',
                                'param_name' => 'tab_content',
                                'heading' => esc_html__('Content', 'gullu-core'),
                                'description' => esc_html__('Put some contents about the plan. (HTML Supported).', 'gullu-core'),
                            ),
                        )
                    )
                ),
            )
        );

    }
});
