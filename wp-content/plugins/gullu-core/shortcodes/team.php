<?php
add_shortcode('gullu_team', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'style' => 'style_01'
    ),$atts);
    $members = vc_param_group_parse_atts($content);
    $style_two = $atts['style']=='style_02' ? 'style-two' : '';
    ?>

        <div class="our-team-styleOne <?php echo $style_two; ?>">
            <div class="row">
                <?php
                foreach($members as $member) {
                    $profile_pic = wp_get_attachment_image_src($member['image'], 'full');
                    ?>
                    <div class="col-md-4 col-xs-6">
                        <div class="single-team-member">
                            <div class="image">
                                <img src="<?php echo $profile_pic[0]; ?>" alt="">
                                <div class="opacity tran3s">
                                    <ul class="tran3s">
                                        <?php
                                        if(!empty($member['fb'])) echo '<li><a href="'.$member['fb'].'" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>';
                                        if(!empty($member['twitter'])) echo '<li><a href="'.$member['twitter'].'" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>';
                                        if(!empty($member['dribbble'])) echo '<li><a href="'.$member['dribbble'].'" class="tran3s"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>';
                                        if(!empty($member['google-plus'])) echo '<li><a href="'.$member['google-plus'].'" class="tran3s"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>';
                                        ?>
                                    </ul>
                                </div>
                            </div> <!-- /.image -->
                            <h6> <?php echo $member['name'] ?> </h6>
                            <p> <?php echo $member['designation']; ?> </p>
                        </div> <!-- /.single-team-member -->
                    </div>
                    <?php
                }
                ?>
            </div> <!-- /.row -->
        </div>


    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Team', 'gullu-core'),
                'description'       => esc_html__('Create team members', 'gullu-core'),
                'base'              => 'gullu_team',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01 (Rectangle border)' => 'style_01',
                            'Style 02 (Rounded border)' => 'style_02',
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Members', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'image',
                                'heading' => esc_html__('Profile picture', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'name',
                                'heading' => esc_html__('Name', 'gullu-core'),
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'designation',
                                'heading' => esc_html__('Designation', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'fb',
                                'heading' => esc_html__('Facebook link', 'gullu-core'),
                                'value' => '#'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'twitter',
                                'heading' => esc_html__('Twitter link', 'gullu-core'),
                                'value' => '#'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'dribbble',
                                'heading' => esc_html__('Dribbble link', 'gullu-core'),
                                'value' => '#'
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'google-plus',
                                'heading' => esc_html__('Google plus link', 'gullu-core'),
                                'value' => '#'
                            ),
                        )
                    )
                ),
            )
        );

    }
});
