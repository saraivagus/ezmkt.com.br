<?php
add_shortcode('gullu_statics_sec', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'statics_image' => '',
        'title'         => 'Business Statics',
        'big_title'     => '<span>We’re <strong>ready to</strong> serve</span> <br> <span>you with best.</span>',
        'top_statics'   => '1,234',
        'btm_statics'   => '786',
        'top_statics_label' => 'Market Growth',
        'btm_statics_label' => 'Market Drop',
    ),$atts);

    $statics_image = wp_get_attachment_image_src($atts['statics_image'], 'full');
    ?>

    <div class="business-statics">
        <img src="<?php echo $statics_image[0]; ?>" alt="" id="chart">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 float-right">
                    <div class="main-content">
                        <div class="theme-title-two">
                            <h6> <?php echo $atts['title']; ?> </h6>
                            <h2> <?php echo $atts['big_title']; ?> </h2>
                        </div>
                        <div class="main-wrapper">
                            <?php echo $content; ?>
                        </div> <!-- /.main-wrapper -->
                    </div> <!-- /.main-content -->
                </div> <!-- /.col- -->

                <div class="col-md-6 col-xs-12">
                    <div class="wrapper">
                        <div class="statics">
                            <strong> <?php echo $atts['top_statics']; ?> </strong>
                            <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
                            <p> <?php echo $atts['top_statics_label']; ?> </p>
                        </div> <!-- /.statics -->
                        <div class="statics fix">
                            <strong> <?php echo $atts['btm_statics']; ?> </strong>
                            <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                            <p> <?php echo $atts['btm_statics_label']; ?> </p>
                        </div> <!-- /.statics -->
                    </div> <!-- /.wrapper -->
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Statistics Section', 'gullu-core'),
                'description'       => esc_html__('Create statistics section easily.', 'gullu-core'),
                'base'              => 'gullu_statics_sec',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'Business Statics',
                        'holder' => 'h2'
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'big_title',
                        'heading' => esc_html__('Big Title', 'gullu-core'),
                        'description' => esc_html__('Wrap every line with <span></span> tag and wrap accent colored text with <strong></strong> tag.', 'gullu-core'),
                        'value' => '<span>We’re <strong>ready to</strong> serve</span> <br> <span>you with best.</span>'
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'statics_image',
                        'heading' => esc_html__('Statistics background image', 'gullu-core'),
                        'description' => esc_html__('Statistics background image maybe a chart image. Look at the demo', 'gullu-core'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'top_statics',
                        'heading' => esc_html__('Top statistics count', 'gullu-core'),
                        'value' => '1,234'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'top_statics_label',
                        'heading' => esc_html__('Top statistics label', 'gullu-core'),
                        'value' => 'Market Growth'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'btm_statics',
                        'heading' => esc_html__('Bottom statistics count', 'gullu-core'),
                        'value' => '786'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'btm_statics_label',
                        'heading' => esc_html__('Bottom statistics label', 'gullu-core'),
                        'value' => 'Market Drop'
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'content',
                        'heading' => esc_html__('Content', 'gullu-core'),
                        'description' => esc_html__('HTML supported.', 'gullu-core'),
                    ),
                ),
            )
        );

    }
});
