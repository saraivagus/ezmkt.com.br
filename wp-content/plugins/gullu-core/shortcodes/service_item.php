<?php
add_shortcode('gullu_service_item', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'top_title' => '',
        'title'     => '',
        'url'       => '',
        'image'     => '',
        'hover1'    =>  '',
        'hover2'    =>  '',
        'align'     => 'left'
    ),$atts);

    $image  = wp_get_attachment_image_src($atts['image'], 'gullu_550x600');

    $align = $atts['align']=='right' ? 'float-right' : '';
    ?>

    <div class="single-service">
        <div class="row">
            <div class="col-md-6 col-xs-12 <?php echo $align; ?>">
                <div class="image-box">
                    <img src="<?php echo $image[0]; ?>" alt="<?php echo $atts['title']; ?>">
                    <div class="opacity tran3s">
                        <p class="one"> <?php echo $atts['hover1'] ?> </p>
                        <p class="two"> <?php echo $atts['hover2'] ?> </p>
                    </div>
                </div>
            </div> <!-- /.col-lg-6 -->
            <div class="col-md-6 col-xs-12 text">
                <p> <?php echo $atts['top_title'] ?> </p>
                <h2> <?php echo $atts['title']; ?> </h2>
                <a href="<?php echo $atts['url'] ?>" class="tran3s"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.single-service -->


    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Single Service', 'gullu-core'),
                'description'       => esc_html__('Create service item with image', 'gullu-core'),
                'base'              => 'gullu_service_item',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'textfield',
                        'param_name' => 'top_title',
                        'heading' => esc_html__('Top Small Title', 'gullu-core'),
                        'holder' => 'h2',
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Main Title', 'gullu-core'),
                        'holder' => 'h2',
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image',
                        'heading' => esc_html__('Featured image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'align',
                        'heading' => esc_html__('Image position', 'gullu-core'),
                        'value' => array(
                            'Left' => 'left',
                            'Right'=> 'right'
                        ),
                        'holder' => 'h2'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'hover1',
                        'heading' => esc_html__('Top hover content', 'gullu-core'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'hover2',
                        'heading' => esc_html__('Bottom hover content', 'gullu-core'),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'url',
                        'heading' => esc_html__('Link', 'gullu-core'),
                        'value' => '#'
                    ),
                ),
            )
        );

    }
});
