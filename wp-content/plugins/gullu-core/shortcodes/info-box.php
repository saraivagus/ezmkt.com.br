<?php
add_shortcode('gullu_item_item', function($atts, $content) {
    ob_start();
    $atts = shortcode_atts(array(
        'title'     => 'Provide financial advise by our advisor',
        'subtitle'  => 'Copywrite, blogpublic realations content translation.',
        'icon'      => 'flaticon-check',
    ),$atts);
    ?>
    <div class="single-block info-box">
        <div class="icon"><i class="flaticon-check"></i></div>
        <h6> <?php echo $atts['title'] ?> </h6>
        <?php if(!empty($atts['subtitle'])) { ?>  <p> <?php echo $atts['subtitle'] ?> </p> <?php } ?>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Info box', 'gullu-core'),
                'description'       => esc_html__('Create info box item.', 'gullu-core'),
                'base'              => 'gullu_item_item',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'iconpicker',
                        'param_name' => 'icon',
                        'settings' => array(
                            'emptyIcon' => false,
                            'type' => 'flaticon',
                            'iconsPerPage' => 200,
                        ),
                        'dependency' => array(
                            'element' => 'icon_type',
                            'value' => 'flaticon',
                        ),
                        'value' => 'flaticon-check',
                        'heading' => esc_html__('Icon', 'gullu-core')
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => 'Provide financial advise by our advisor'
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'subtitle',
                        'heading' => esc_html__('Sub Title', 'gullu-core'),
                        'value' => 'Copywrite, blogpublic realations content translation.'
                    ),
                ),
            )
        );

    }
});
