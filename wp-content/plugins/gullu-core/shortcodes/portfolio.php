<?php
add_shortcode('gullu_portfolio', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'title'         => 'We’ve done lot’s of work, Let’s Check some from here',
        'small_title'   => 'RECENT WORK',
        'ppp'           => 6,
        'btn'           => '',
        'style'         => 'style_01',
        'link'          => 'thumb',
        'padding'       => '180px 0 200px 0',
    ),$atts);

    $btn    = vc_build_link($atts['btn']);
    $portfolios = new WP_Query(array(
        'post_type' => 'portfolio',
        'posts_per_page' => $atts['ppp']
    ));
    
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="our-portfolio" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="container">
                <div class="theme-title">
                    <h6> <?php echo $atts['small_title']; ?> </h6>
                    <h2> <?php echo $atts['title']; ?> </h2>
                    <a href="<?php echo $btn['url']; ?>" class="tran3s"> <?php echo $btn['title']; ?> </a>
                </div>
            </div>
            <div class="wrapper">
                <div class="row">
                    <div class="portfolio-slider">
                        <?php
                        while ($portfolios->have_posts()) : $portfolios->the_post();
                            $link = $atts['link']=='thumb' ? get_the_post_thumbnail_url() : get_the_permalink();
                            $fancybox = $atts['link']=='thumb' ? 'data-fancybox="project"' : '';
                            ?>
                            <div class="item">
                                <div class="image">
                                    <?php the_post_thumbnail('gullu_430x431'); ?>
                                    <div class="opacity tran4s">
                                        <a <?php echo $fancybox; ?> href="<?php echo $link ?>"
                                           target="<?php echo $btn['target']; ?>" class="tran3s"
                                           title="<?php the_title(); ?>"></a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }


    elseif($atts['style']=='style_02') {
        $cats = get_terms('portfolio_cat');
        wp_enqueue_script('mixitup');
        ?>
        <div class="gullu-portfolio portfolio-full-width" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="container">
                <div class="mixitUp-menu">
                    <h2> <?php echo $atts['title'] ?> </h2>
                    <ul>
                        <li class="filter active tran3s" data-filter="all"> <?php esc_html_e('All', 'gullu-core'); ?> </li>
                        <?php
                        $count = count($cats);
                        if ( $count > 0 ) {
                            foreach($cats as $cat) {
                                echo  '<li class="filter tran3s" data-filter=".'.$cat->slug.'">' . $cat->name . '</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="row" id="mixitUp-item">
                <?php
                while($portfolios->have_posts()) : $portfolios->the_post();          
                $portfolio_metaboxes = get_post_meta(get_the_ID(), 'portfolio_metaboxes', true);
                $video_url = isset($portfolio_metaboxes['video_url']) ? $portfolio_metaboxes['video_url'] : '';
                    $the_cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                    $cat_slugs = '';
                    if(is_array($the_cats)) {
                        foreach ($the_cats as $the_cat) {
                            $cat_slugs .= $the_cat->slug . ' ';
                        }
                    }
                    ?>
                    <div class="col-md-4 col-xs-6 mix <?php echo $cat_slugs; ?>">
                        <div class="single-item">
                            <?php the_post_thumbnail('gullu_640x500') ?>
                            <div class="opacity tran3s">
                                <h5><a href="<?php the_permalink() ?>" class="tran3s"> <?php the_title(); ?> </a></h5>
                                <?php 
                                if(has_post_format('video')) { ?>
                                    <a data-fancybox href="<?php echo $video_url; ?>?rel=0&amp;showinfo=0" class="view-more tran3s"> <i class="flaticon-play-button"></i> </a> <?php
                                }else { ?>
                                    <a href="<?php the_permalink();; ?>" class="view-more tran3s"> <i class="flaticon-plus"></i> </a> <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
        <?php
    }


    elseif($atts['style']=='style_03') {
        $cats = get_terms('portfolio_cat');
        wp_enqueue_script('mixitup');
        ?>
        <div class="gullu-portfolio portfolio-grid" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="container">
                <div class="mixitUp-menu">
                    <h2> <?php echo $atts['title'] ?> </h2>
                    <ul>
                        <li class="filter active tran3s" data-filter="all"> <?php esc_html_e('All', 'gullu-core'); ?> </li>
                        <?php
                        $count = count($cats);
                        if ( $count > 0 ) {
                            foreach($cats as $cat) {
                                echo  '<li class="filter tran3s" data-filter=".'.$cat->slug.'">' . $cat->name . '</li>';
                            }
                        }
                        ?>
                    </ul>
                </div> <!-- End of .mixitUp-menu -->

                <div class="row" id="mixitUp-item">
                    <?php
                    while($portfolios->have_posts()) : $portfolios->the_post();
                        $the_cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                        $cat_slugs = '';
                        if(is_array($the_cats)) {
                            foreach ($the_cats as $the_cat) {
                                $cat_slugs .= $the_cat->slug . ' ';
                            }
                        }
                        ?>
                        <div class="col-xs-6 mix <?php echo $cat_slugs; ?>">
                            <div class="single-item">
                                <?php the_post_thumbnail('gullu_570x480') ?>
                                <div class="opacity tran3s">
                                    <a href="<?php the_permalink(); ?>" class="view-more tran3s">
                                        <?php echo has_post_format('video') ? '<i class="flaticon-play-button"></i>' : '<i class="flaticon-plus"></i>'; ?>
                                    </a>
                                </div>
                            </div> <!-- /.single-item -->
                        </div> <!-- /.col-md-6 -->
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <?php
    }

    elseif($atts['style']=='style_04') {
        $cats = get_terms('portfolio_cat');
        wp_enqueue_script('mixitup');
        ?>
        <div class="gullu-portfolio portfolio-full-width p-bottm0" style="padding:<?php echo $atts['padding'] ?>;">
            <div class="container">
                <div class="mixitUp-menu">
                    <h2> <?php echo $atts['title']; ?> </h2>
                    <ul>
                        <li class="filter active tran3s" data-filter="all"> <?php esc_html_e('All', 'gullu-core'); ?> </li>
                        <?php
                        $count = count($cats);
                        if ( $count > 0 ) {
                            foreach($cats as $cat) {
                                echo  '<li class="filter tran3s" data-filter=".'.$cat->slug.'">' . $cat->name . '</li>';
                            }
                        }
                        ?>
                    </ul>
                </div> <!-- End of .mixitUp-menu -->
            </div> <!-- /.container -->

            <div class="row" id="mixitUp-item">
                <?php
                while($portfolios->have_posts()) : $portfolios->the_post();
                    $the_cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                    $cat_slugs = '';
                    if(is_array($the_cats)) {
                        foreach ($the_cats as $the_cat) {
                            $cat_slugs .= $the_cat->slug . ' ';
                        }
                    }
                    ?>
                    <div class="col-xs-6 mix <?php echo $cat_slugs; ?>">
                        <div class="single-item">
                            <?php the_post_thumbnail('gullu_960x580') ?>
                            <div class="opacity tran3s">
                                <a href="<?php the_permalink(); ?>" class="view-more tran3s">
                                    <?php echo has_post_format('video') ? '<i class="flaticon-play-button"></i>' : '<i class="flaticon-plus"></i>'; ?>
                                </a>
                            </div>
                        </div> <!-- /.single-item -->
                    </div> <!-- /.col-md-6 -->
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div> <!-- /.row -->
        </div>
        <?php
    }


    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Portfolio', 'gullu-core'),
                'description'       => esc_html__('Display portfolio section', 'gullu-core'),
                'base'              => 'gullu_portfolio',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01 (Carousel)' => 'style_01',
                            'Style 02 (Full width 3 column)' => 'style_02',
                            'Style 03 (Boxed width 2 column)' => 'style_03',
                            'Style 04 (Full width 2 column)' => 'style_04',
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'We’ve done lot’s of work, Let’s Check some from here',
                        'holder' => 'h2',
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'small_title',
                        'heading' => esc_html__('Small title', 'gullu-core'),
                        'value' => 'RECENT WORK',
                        'dependency' => array(
                            'element' => 'style',
                            'value'   => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'link',
                        'heading' => esc_html__('Link to', 'gullu-core'),
                        'description' => esc_html__('Which link will open when user click on the portfolio thumbnail?', 'gullu-core'),
                        'value' => array(
                            'Thumbnail' => 'thumb',
                            'Single portfolio' => 'single_portfolio',
                        ),
                        'dependency' => array(
                            'element' => 'style',
                            'value'   => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'ppp',
                        'heading' => esc_html__('Posts show count', 'gullu-core'),
                        'value' => 6,
                    ),
                    array(
                        'type' => 'vc_link',
                        'param_name' => 'btn',
                        'heading' => esc_html__('Button', 'gullu-core'),
                        'dependency' => array(
                            'element' => 'style',
                            'value'   => 'style_01'
                        )
                    ),
                    // Group: Styling
                    array(
                        'type' => 'textfield',
                        'param_name' => 'padding',
                        'heading' => 'Padding',
                        'description' => 'Padding around the section. Input the padding as clock wise (Top Right Bottom Left)',
                        'value' => '180px 0 200px 0',
                        'group' => 'Styling'
                    ),
                ),
            )
        );

    }
});
