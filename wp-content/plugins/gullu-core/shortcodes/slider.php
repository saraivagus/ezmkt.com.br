<?php
add_shortcode('gullu_slider', function($atts, $content) {
    ob_start();
    $atts = shortcode_atts(array(
        'overlay_image' => '',
    ),$atts);
    $slider_items = vc_param_group_parse_atts($content);
    $overlay_image = wp_get_attachment_image_src($atts['overlay_image'], 'full');
    add_action( 'wp_footer', function() use($overlay_image) {
        $css = '';
        if ( isset($overlay_image[0]) ) {
            $css = "#theme-main-banner.banner-one .camera_overlayer {
                        background: url($overlay_image[0]) no-repeat center center;
                    }";
        }
        wp_register_style( 'gullu_slider', false );
        wp_enqueue_style( 'gullu_slider' );
        wp_add_inline_style( 'gullu_slider', $css );
    });
    ?>

    <div id="theme-main-banner" class="banner-one">
        <?php
        foreach($slider_items as $slider_item) {
            $btn = vc_build_link($slider_item['link']);
            $bg_image = wp_get_attachment_image_src($slider_item['bg_image'], 'full');
            $slide_image = wp_get_attachment_image_src($slider_item['slide_image'], 'full');
            $slide_bg = wp_get_attachment_image_src($slider_item['slide_bg'], 'full');
            $slide_bg = isset($slide_bg[0]) ? $slide_bg[0] : '';
            ?>
            <div data-src="<?php echo $bg_image[0]; ?>">
                <div class="camera_caption">
                    <div class="container">
                        <h5 class="wow fadeInUp animated"> <?php echo $slider_item['title']; ?> </h5>
                        <h1 class="wow fadeInUp animated" data-wow-delay="0.2s">
                            <?php echo $slider_item['btm_title']; ?>
                        </h1>
                        <a href="<?php echo $btn['url']; ?>" class="tran3s hvr-trim wow fadeInUp animated p-bg-color button-one" data-wow-delay="0.3s"> <?php echo $btn['title']; ?> </a>
                        <div class="wow fadeInRight animated image-shape-one" data-wow-delay="0.33s">
                            <img src="<?php echo $slide_image[0]; ?>" class="banner-img-one"/>
                        </div>
                        <div class="wow fadeInRight animated image-shape-two" data-wow-delay="0.39s"><div class="theme-shape-two" style="background: url('<?php echo $slide_bg ?>');"></div></div>
                    </div> <!-- /.container -->
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <?php
    $html = ob_get_clean();

    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Slider', 'gullu-core'),
                'description'       => esc_html__('Create Gullu slider', 'gullu-core'),
                'base'              => 'gullu_slider',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'overlay_image',
                        'heading' => esc_html__('Overlay Image', 'gullu-core'),
                        'description' => esc_html__('The overlay image will be applied on all of the slides and it will appear at the top of the background image. We recommend to use here a PNG image.', 'gullu-core')
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Slider Item', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'title',
                                'value' => 'We’r Awesome',
                                'heading' => esc_html__('Top Title', 'gullu-core'),
                                'description' => esc_html__('Top Title is the small title.', 'gullu-core'),
                            ),
                            array(
                                'type' => 'textarea',
                                'param_name' => 'btm_title',
                                'value' => '<span>Digital Agency</span><br><span>That Help You to</span><br><span>Go Ahead</span>',
                                'heading' => esc_html__('Bottom title', 'gullu-core'),
                                'description' => esc_html__('This is the big title. Wrap every line with <span></span> tag and use a <br> tag after line ending. See the default title text as example.', 'gullu-core'),
                            ),
                            array(
                                'type' => 'vc_link',
                                'param_name' => 'link',
                                'value' => '#',
                                'heading' => esc_html__('Button', 'gullu-core')
                            ),
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'bg_image',
                                'heading' => esc_html__('Background Image', 'gullu-core')
                            ),
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'slide_image',
                                'heading' => esc_html__('Slide Image', 'gullu-core')
                            ),
                            array(
                                'type' => 'attach_image',
                                'param_name' => 'slide_bg',
                                'heading' => esc_html__('Slide background image', 'gullu-core')
                            ),
                        )
                    )
                ),
            )
        );

    }
});
