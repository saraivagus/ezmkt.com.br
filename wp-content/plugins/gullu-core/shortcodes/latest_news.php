<?php
add_shortcode('gullu_news', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'title'         => 'News from our company',
        'small_title'   => 'Our News',
        'ppp'           => 4,
        'read_more'     => 'More Details',
        'order'         => 'ASC',
        'cats'          => '',
        'style'         => 'style_01',
    ),$atts);

    $posts = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => $atts['ppp'],
        'order' => $atts['order'],
        'category_name' => $atts['cats']
    ));
    ?>
    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="home-blog-section">
            <div class="theme-title text-center">
                <h6> <?php echo $atts['small_title']; ?> </h6>

                <h2> <?php echo $atts['title']; ?> </h2>
            </div>
            <!-- /.theme-title -->
            <div class="row">
                <?php
                $i = 1;
                while ($posts->have_posts()) : $posts->the_post();
                    $post_metaboxes = get_post_meta(get_the_ID(), 'post_metaboxes', true);
                    $post_color = isset($post_metaboxes['post_color']) ? $post_metaboxes['post_color'] : '';
                    ?>
                    <style>
                        .home-blog-section .single-blog.color<?php echo $i; ?> h5 a:before {
                            background: <?php echo $post_color; ?>;
                        }
                    </style>
                    <div class="col-md-6 col-xs-12">
                        <div class="single-blog color<?php echo $i; ?>">
                            <img src="<?php echo get_avatar_url(get_the_author_meta('user_email'), 70); ?>"
                                 alt="<?php echo get_the_author_meta('display_name'); ?>">
                            <h6> <?php echo get_the_author_meta('display_name'); ?> </h6>
                            <h5><a href="<?php the_permalink(); ?>" class="tran3s"
                                   title="<?php the_title() ?>"> <?php echo wp_trim_words(get_the_title(), 6, '') ?> </a>
                            </h5>

                            <p> <?php echo wp_trim_words(get_the_content(), 22, ''); ?> </p>
                            <a href="<?php the_permalink(); ?>"
                               class="tran3s hvr-icon-wobble-horizontal"> <?php echo $atts['read_more']; ?> <i
                                    class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <!-- /.single-blog -->
                    </div> <!-- /.col- -->
                    <?php
                    $i++;
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
            <!-- /.row -->
        </div>
        <?php
    }

    if($atts['style']=='style_02') {
        ?>
        <div class="home-project">
            <div class="row">
                <?php
                unset($i);
                $i = 1;
                while ($posts->have_posts()) : $posts->the_post();
                    $number = sprintf("%02d", $i);
                    $space = $i%2 != 0 ? 'space' : '';
                    ?>
                    <div class="col-md-6 col-xs-12">
                        <div class="single-project-wrapper">
                            <?php echo $i%2 == 0 ? get_the_post_thumbnail(get_the_ID(), 'gullu_460x620', array('class'=> 'space')) : ''; ?>
                            <div class="text <?php echo $space; ?>">
                                <h6> <?php Gullu_first_category(); ?> </h6>
                                <h2><a href="<?php the_permalink(); ?>" class="tran3s"> <?php the_title(); ?> </a></h2>
                                <div class="content">
                                    <span><?php echo $number; ?></span>
                                    <p> <?php echo wp_trim_words(get_the_content(), 20, ''); ?> </p>
                                    <a href="<?php the_permalink(); ?>" class="tran3s"> <?php echo $atts['read_more']; ?> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div> <!-- /.content -->
                            </div> <!-- /.text -->
                            <?php echo $i%2 != 0 ? get_the_post_thumbnail(get_the_ID(), 'gullu_460x620') : ''; ?>
                        </div> <!-- /.single-project-wrapper -->
                    </div>
                    <?php
                    $i++;
                endwhile;
                wp_reset_postdata();
                ?>
            </div> <!-- /.row -->
        </div>
        <?php
    }
    ?>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Blog Posts', 'gullu-core'),
                'description'       => esc_html__('Display blog posts', 'gullu-core'),
                'base'              => 'gullu_news',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                        ),
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'value' => 'News from our company',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'small_title',
                        'heading' => esc_html__('Small title', 'gullu-core'),
                        'value' => 'RECENT WORK',
                        'holder' => 'h2',
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_01'
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'ppp',
                        'heading' => esc_html__('Posts show count', 'gullu-core'),
                        'value' => 4,
                    ),
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'order',
                        'heading' => esc_html__('Post order', 'gullu-core'),
                        'value' => array(
                            'Ascending' => 'ASC',
                            'Descending ' => 'DESC',
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'cats',
                        'heading' => esc_html__('Category', 'gullu-core'),
                        'value' => Gullu_cat_array()
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'read_more',
                        'heading' => esc_html__('Read more link label', 'gullu-core'),
                        'value' => 'More Details'
                    ),
                ),
            )
        );

    }
});
