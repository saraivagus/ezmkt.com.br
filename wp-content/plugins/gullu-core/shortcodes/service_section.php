<?php
add_shortcode('gullu_service_sec', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'style' => 'style_01',
        'title' => 'We provide wide range of web & business services.',
        'small_title' => 'OUR SERVICES',
        'subtitle' => 'We’ve strong work history with different business services',
        'btn' => '',
        'image' => '',
        'padding' => '',
        'margin_subtitle' => '50px 0 130px 0',
    ),$atts);

    $service_items = vc_param_group_parse_atts($content);
    $btn = vc_build_link($atts['btn']);
    $image = wp_get_attachment_image_src($atts['image'], 'full');
    ?>

    <?php
    if($atts['style']=='style_01') {
        ?>
        <div class="what-we-do" style="padding: <?php echo $atts['padding'] ?>;">
            <h3> <?php echo $atts['title']; ?> </h3>
            <h6 style="margin: <?php echo $atts['margin_subtitle'] ?>;"> <?php echo $atts['subtitle']; ?> </h6>
            <div class="row">
                <?php
                foreach ($service_items as $service_item) {
                    $link = isset($service_item['link']) ? $service_item['link'] : '';
                    $link = vc_build_link($link);
                    $icon_type = !empty($service_item['icon_type']) ? $service_item['icon_type'] :  '';
                    $icon = $icon_type=='flaticon' ? $service_item['icon'] : $service_item['fontawesome'];
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 wow fadeInLeft">
                        <div class="single-block">
                            <div class="icon color-one">
                                <i class="<?php echo $icon; ?>" style="color:<?php echo $service_item['icon-color']; ?>"></i>
                            </div>
                            <h6> <?php echo $service_item['title'] ?> </h6>
                            <h5> <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="tran3s"> <?php echo $link['title']; ?> </a> </h5>
                        </div>
                        <!-- /.single-block -->
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <?php
    }

    elseif($atts['style']=='style_02') {
        ?>
        <div class="home-service-section" style="padding: <?php echo $atts['padding'] ?>;">
            <div class="container">
                <div class="col-md-9 col-md-offset-3 main-container">
                    <div class="theme-title">
                        <h6> <?php echo $atts['small_title']; ?> </h6>
                        <h2><?php echo $atts['title']; ?></h2>
                        <p> <?php echo $atts['subtitle']; ?> </p>
                        <a href="<?php echo $btn['url']; ?>" target="<?php echo $btn['target']; ?>" class="tran3s"> <?php echo $btn['title']; ?> </a>
                    </div> <!-- /.theme-title -->
                    <ul class="clearfix row">
                        <?php
                        foreach ($service_items as $service_item) {
                            $link = vc_build_link($service_item['link']);
                            ?>
                            <li class="col-md-6">
                                <div>
                                    <i class="<?php echo $service_item['icon']; ?>" style="color:<?php echo $service_item['icon-color']; ?>"></i>
                                    <h5><a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="tran3s"> <?php echo $link['title']; ?> </a></h5>
                                    <p> <?php echo $service_item['title'] ?> </p>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div> <!-- /.main-container -->
                <img src="<?php echo $image[0]; ?>" alt="Image" class="wow fadeInLeft">
            </div> <!-- /.container -->
        </div>
        <?php
    }

    elseif($atts['style']=='style_03') {
        ?>
        <div class="service-version-one" style="padding: <?php echo $atts['padding'] ?>;">
            <h2> <?php echo $atts['title']; ?> </h2>
            <div class="row">
                <?php
                foreach ($service_items as $service_item) {
                    $link = vc_build_link($service_item['link']);
                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-service">
                            <i class="<?php echo $service_item['icon']; ?> tran3s" style="color:<?php echo $service_item['icon-color']; ?>"></i>
                            <p> <?php echo $service_item['title'] ?> </p>
                            <h6><a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="tran3s"> <?php echo $link['title']; ?> </a></h6>
                        </div> <!-- /.single-service -->
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php
    }


    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {

        // Add new custom font to Font Family selection in icon box module
        function zeckart_add_new_icon_set_to_iconbox() {
            $param = WPBMap::getParam('vc_icon', 'type');
            $param['value'][__('FlatIcon', 'total')] = 'gullu-flaticon';
            vc_update_shortcode_param('vc_icon', $param);
        }

        add_filter('init', 'zeckart_add_new_icon_set_to_iconbox', 40);


        // Add array of your fonts so they can be displayed in the font selector
        function zeckart_icon_array() {
            return array(
                array('flaticon-arrows' => 'Arrows'),
                array('flaticon-computer' => 'Computer'),
                array('flaticon-notepad' => 'Notepad'),
                array('flaticon-diamond' => 'Diamond'),
                array('flaticon-right-arrow' => 'Right Arrow'),
                array('flaticon-download-arrow' => 'Download Arrow'),
                array('flaticon-up-arrow' => 'Up Arrow'),
                array('flaticon-user' => 'User'),
                array('flaticon-round-chart' => 'Round chart'),
                array('flaticon-quote-left' => 'Quot left'),
                array('flaticon-note' => 'Note'),
                array('flaticon-plus' => 'Plus'),
                array('flaticon-bar-chart' => 'Bar chart'),
                array('flaticon-check' => 'Check'),
                array('flaticon-drawing' => 'Drawing'),
                array('flaticon-layers' => 'Layers'),
                array('flaticon-smartphone' => 'Smartphone'),
                array('flaticon-business' => 'Business'),
                array('flaticon-bar-chart2' => 'Bar chart 2'),
                array('flaticon-square' => 'Square'),
                array('flaticon-play-button' => 'Play button'),
            );
        }

        add_filter('vc_iconpicker-type-flaticon', 'zeckart_icon_array');


        /**
         * Register Backend and Frontend CSS Styles
         */
        add_action('vc_base_register_front_css', 'zeckart_vc_iconpicker_base_register_css');
        add_action('vc_base_register_admin_css', 'zeckart_vc_iconpicker_base_register_css');
        function zeckart_vc_iconpicker_base_register_css() {
            wp_register_style('gullu-flaticon', get_stylesheet_directory_uri() . '/assets/fonts/icon/font/flaticon.css');
        }

        /**
         * Enqueue Backend and Frontend CSS Styles
         */
        add_action('vc_backend_editor_enqueue_js_css', 'zeckart_vc_iconpicker_editor_jscss');
        add_action('vc_frontend_editor_enqueue_js_css', 'zeckart_vc_iconpicker_editor_jscss');
        function zeckart_vc_iconpicker_editor_jscss() {
            wp_enqueue_style('gullu-flaticon');
        }

        /**
         * Enqueue CSS in Frontend when it's used
         */
        add_action('vc_enqueue_font_icon_element', 'zeckart_enqueue_font_icomoon');
        function zeckart_enqueue_font_icomoon($font) {
            switch ($font) {
                case 'gullu-flaticon':
                    wp_enqueue_style('gullu-flaticon');
            }
        }

        vc_map(array(
                'name'              => esc_html__('Service Section', 'gullu-core'),
                'description'       => esc_html__('Create service section.', 'gullu-core'),
                'base'              => 'gullu_service_sec',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'style',
                        'heading' => esc_html__('Style', 'gullu-core'),
                        'admin_label' => true,
                        'value' => array(
                            'Style 01' => 'style_01',
                            'Style 02' => 'style_02',
                            'Style 03' => 'style_03',
                        )
                    ),
                    array(
                        'type' => 'textarea',
                        'param_name' => 'title',
                        'heading' => esc_html__('Title', 'gullu-core'),
                        'holder' => 'h2',
                        'value' => 'We provide wide range of web & business services.'
                    ),
                    array(
                        'type' => 'text',
                        'param_name' => 'small_title',
                        'heading' => esc_html__('Small Title', 'gullu-core'),
                        'description' => esc_html__('Small title will show on the top.', 'gullu-core'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_02'
                        ),
                        'value' => 'OUR SERVICES'
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'subtitle',
                        'heading' => esc_html__('Subtitle', 'gullu-core'),
                        'value' => 'We’ve strong work history with different business services'
                    ),
                    array(
                        'type' => 'vc_link',
                        'param_name' => 'btn',
                        'heading' => esc_html__('Button', 'gullu-core'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_02'
                        )
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image',
                        'heading' => esc_html__('Left Image', 'gullu-core'),
                        'dependency' => array(
                            'element' => 'style',
                            'value' => 'style_02'
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'content',
                        'heading' => esc_html__('Service Items', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'title',
                                'heading' => esc_html__('Text', 'gullu-core'),
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'vc_link',
                                'param_name' => 'link',
                                'heading' => esc_html__('Link text', 'gullu-core')
                            ),
                            array(
                                'type' => 'dropdown',
                                'param_name' => 'icon_type',
                                'value' => array(
                                    'Font-awesome' => 'fontawesome',
                                    'FlatIcon' => 'flaticon'
                                ),
                                'std' => 'flaticon',
                                'heading' => esc_html__('Icon type', 'gullu-core')
                            ),
                            array(
                                'type' => 'iconpicker',
                                'param_name' => 'fontawesome',
                                'settings' => array(
                                    'emptyIcon' => false,
                                    'type' => 'fontawesome',
                                    'iconsPerPage' => 500,
                                ),
                                'dependency' => array(
                                    'element' => 'icon_type',
                                    'value' => 'fontawesome',
                                ),
                                'heading' => esc_html__('Font-awesome Icon', 'gullu-core')
                            ),
                            array(
                                'type' => 'iconpicker',
                                'param_name' => 'icon',
                                'settings' => array(
                                    'emptyIcon' => false,
                                    'type' => 'flaticon',
                                    'iconsPerPage' => 200,
                                ),
                                'dependency' => array(
                                    'element' => 'icon_type',
                                    'value' => 'flaticon',
                                ),
                                'heading' => esc_html__('FlatIcon', 'gullu-core')
                            ),
                            array(
                                'type' => 'colorpicker',
                                'param_name' => 'icon-color',
                                'heading' => esc_html__('Icon color', 'gullu-core'),
                                'value' => '#00d747'
                            ),
                        )
                    ),
                    array(
                        'type' => 'textfield',
                        'param_name' => 'margin_subtitle',
                        'heading' => esc_html__('Margin around the subtitle', 'gullu-core'),
                        'description' => esc_html__('Input the margin as clock wise (Top Right Bottom Left)', 'gullu-core'),
                        'group' => 'Styling',
                        'value' => '50px 0 130px 0'
                    ),
                ),
            )
        );

    }
});
