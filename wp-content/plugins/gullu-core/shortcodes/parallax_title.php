<?php
add_shortcode('gullu_parallax', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'btn'       => '',
        'bg_image'  => '#0dda8f',
        'bg_overlay'  => 'rgba(0,0,0,0.75)',
    ),$atts);
    $bg_image = wp_get_attachment_image_src($atts['bg_image'], 'full');
    $bg_image = "style=\"background: url({$bg_image[0]}) no-repeat center; background-size: cover; background-attachment: fixed;\"";
    $bg_overlay = "style=\"background: {$atts['bg_overlay']};\"";
    ?>
    <div <?php echo $bg_image; ?> class="short-banner">
        <div <?php echo $bg_overlay; ?> class="opacity">
            <div class="container">
                <?php echo $content ?>
            </div> <!-- /.container -->
        </div> <!-- /.opacity -->
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Parallax Content', 'gullu-core'),
                'description'       => esc_html__('Create full width parallax content with background image.', 'gullu-core'),
                'base'              => 'gullu_parallax',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'bg_image',
                        'heading' => esc_html__('Background image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'colorpicker',
                        'param_name' => 'bg_overlay',
                        'heading' => esc_html__('Background overlay color', 'gullu-core'),
                        'value' => 'rgba(0,0,0,0.75)'
                    ),
                    array(
                        'type' => 'textarea_html',
                        'param_name' => 'content',
                        'heading' => esc_html__('Content', 'gullu-core'),
                        'holder' => 'h2'
                    ),
                ),
            )
        );

    }
});