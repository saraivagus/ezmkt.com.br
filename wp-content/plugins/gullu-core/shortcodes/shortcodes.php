<?php

// Slider
require_once plugin_dir_path(__FILE__) . 'slider.php';

// Service Section
require_once plugin_dir_path(__FILE__) . 'service_section.php';

// About us
require_once plugin_dir_path(__FILE__) . 'about_us.php';

// Counter
require_once plugin_dir_path(__FILE__) . 'counter.php';

// Portfolio
require_once plugin_dir_path(__FILE__) . 'portfolio.php';

// Testimonial
require_once plugin_dir_path(__FILE__) . 'testimonial.php';

// Pricing
require_once plugin_dir_path(__FILE__) . 'pricing.php';

// Latest News
require_once plugin_dir_path(__FILE__) . 'latest_news.php';

// Video slider
require_once plugin_dir_path(__FILE__) . 'video-slider.php';

// Tab
require_once plugin_dir_path(__FILE__) . 'tab.php';

// Title
require_once plugin_dir_path(__FILE__) . 'title.php';

// Team
require_once plugin_dir_path(__FILE__) . 'team.php';

// Client logos
require_once plugin_dir_path(__FILE__) . 'logos_section.php';

// Service item
require_once plugin_dir_path(__FILE__) . 'service_item.php';

// Info box item
require_once plugin_dir_path(__FILE__) . 'info-box.php';

// Button
require_once plugin_dir_path(__FILE__) . 'button.php';

// Parallax Title
require_once plugin_dir_path(__FILE__) . 'parallax_title.php';

// Statistics section
require_once plugin_dir_path(__FILE__) . 'statics.php';