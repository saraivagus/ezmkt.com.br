<?php
add_shortcode('gullu_tab', function($atts, $content) {
    ob_start();

    $atts = shortcode_atts(array(
        'image' => '',
        'tab_names' => '',
        'tab_contents' => ''
    ),$atts);

    $image = wp_get_attachment_image_src($atts['image'], 'full');
    $tab_names = vc_param_group_parse_atts($atts['tab_names']);
    $tab_contents = vc_param_group_parse_atts($atts['tab_contents']);
    ?>
    <div class="about-text">
        <div class="title">
            <?php echo $content; ?>
        </div>
        <img src="<?php echo $image[0]; ?>" alt="">
        <div class="about-tab-wrapper clearfix">
            <ul class="nav nav-tabs float-left">
                <?php
                $i = 0;
                foreach($tab_names as $tab_name) {
                    $active = $i==0 ? 'class="active"' : '';
                    echo '<li '.$active.'><a data-toggle="tab" href="#'.$tab_name['id'].'">'.$tab_name['name'].'</a></li>';
                    $i++;
                }
                unset($i);
                ?>
            </ul>
            <div class="tab-content float-left">
                <?php
                $i = 0;
                foreach($tab_contents as $tab_content) {
                    $active = $i==0 ? 'active' : ''; ?>
                    <div id="<?php echo $tab_content['id']; ?>" class="tab-pane fade in <?php echo $active; ?>">
                        <?php echo $tab_content['tab_content']; ?>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>
        </div> <!-- /.about-tab-wrapper -->
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});



// VC Config
add_action( 'vc_before_init', function() {
    if( function_exists('vc_map') ) {
        vc_map(array(
                'name'              => esc_html__('Gullu Tab', 'gullu-core'),
                'description'       => esc_html__('Display tabbed content with title.', 'gullu-core'),
                'base'              => 'gullu_tab',
                'category'          => esc_html__('Gullu', 'gullu-core'),
                'params'            => array(
                    array(
                        'type' => 'textarea_html',
                        'param_name' => 'content',
                        'heading' => esc_html__('Title contents.', 'gullu-core'),
                        'description' => esc_html__('Place heading text here. HTML supported', 'gullu-core'),
                    ),
                    array(
                        'type' => 'attach_image',
                        'param_name' => 'image',
                        'heading' => esc_html__('Featured image', 'gullu-core'),
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'tab_names',
                        'heading' => esc_html__('Tab items', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'id',
                                'heading' => esc_html__('Tab ID', 'gullu-core'),
                                'description' => esc_html__('ID should be an unique name (without space).', 'gullu-core'),
                                'value' => 'tab1',
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textfield',
                                'param_name' => 'name',
                                'heading' => esc_html__('Tab Name', 'gullu-core'),
                                'value' => 'Our History',
                                'admin_label' => true
                            ),
                        )
                    ),
                    array(
                        'type' => 'param_group',
                        'value' => '',
                        'param_name' => 'tab_contents',
                        'heading' => esc_html__('Tab Contents', 'gullu-core'),
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'param_name' => 'id',
                                'heading' => esc_html__('Tab ID', 'gullu-core'),
                                'description' => esc_html__('ID should be an unique name (without space).', 'gullu-core'),
                                'value' => 'tab1',
                                'admin_label' => true
                            ),
                            array(
                                'type' => 'textarea',
                                'param_name' => 'tab_content',
                                'heading' => esc_html__('Tab content', 'gullu-core'),
                                'description' => esc_html__('HTML tags supported. You can also include here images with HTML.', 'gullu-core'),
                            ),
                        )
                    )
                ),
            )
        );

    }
});