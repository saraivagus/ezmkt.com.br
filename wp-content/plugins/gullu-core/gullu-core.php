<?php
/**
 * Plugin Name: Gullu Core
 * Plugin URI: http://wordpress.creativegigs.net/gullu/
 * Description: This plugin adds the core features to the Gullu WordPress theme. You must have to install this plugin to work with this theme.
 * Version: 1.7
 * Author: CreativeGigs
 * Author URI: http://creativegigs.net
 * Text domain: gullu-core
 */

if ( !defined('ABSPATH') )
    die('-1');


/**
 * Defining plugin constants
 */
if(!defined('GULLU_CORE_FILE')) {
    define('GULLU_CORE_FILE', plugin_dir_path(__FILE__));
}


/**
 * Register the plugin text domain
 *
 * @return void
 */
add_action( 'plugins_loaded', function() {
    load_plugin_textdomain( 'gullu-core', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
} );


// Custom Functions
require GULLU_CORE_FILE . '/inc/extra.php';
require GULLU_CORE_FILE . '/inc/cp.php';


// Require the Shortcode main funcion File
require GULLU_CORE_FILE . '/shortcodes/shortcodes.php';
require GULLU_CORE_FILE . '/inc/vc_config.php';