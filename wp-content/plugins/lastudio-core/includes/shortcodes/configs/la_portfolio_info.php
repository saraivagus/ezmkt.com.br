<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

if ( !class_exists( 'WPBakeryShortCode_la_portfolio_info' ) ) {
    class WPBakeryShortCode_la_portfolio_info extends LaStudio_Shortcodes_Abstract{

    }
}

$shortcode_params = array(
    array(
        'type' => 'checkbox',
        'heading' => __('Select element to appear', 'la-studio'),
        'param_name' => 'advanced_opts',
        'value' => array(
            __('Tags','la-studio') => 'tag',
            __('Client','la-studio') => 'client',
            __('Category','la-studio') => 'category',
            __('Date','la-studio') => 'date',
            __('Share','la-studio') => 'share'
        )
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Label', 'la-studio'),
        'param_name' 	=> 'tag_label',
        'value' 		=> 'Tags',
        'group' 		=> 'Tags'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Label', 'la-studio'),
        'param_name' 	=> 'client_label',
        'group' 		=> 'Client',
        'value' 		=> 'Client'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Value', 'la-studio'),
        'param_name' 	=> 'client_value',
        'group' 		=> 'Client'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Label', 'la-studio'),
        'param_name' 	=> 'category_label',
        'group' 		=> 'Category',
        'value' 		=> 'Category'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Label', 'la-studio'),
        'param_name' 	=> 'date_label',
        'group' 		=> 'Date',
        'value' 		=> 'Date'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Value', 'la-studio'),
        'param_name' 	=> 'date_value',
        'group' 		=> 'Date'
    ),
    array(
        'type' 			=> 'textfield',
        'heading' 		=> __('Label', 'la-studio'),
        'param_name' 	=> 'share_label',
        'group' 		=> 'Share',
        'value' 		=> 'SHARE'
    ),
    LaStudio_Shortcodes_Helper::fieldExtraClass()
);

return apply_filters(
    'LaStudio/shortcodes/configs',
    array(
        'name'			=> __('Portfolio Information', 'la-studio'),
        'base'			=> 'la_portfolio_info',
        'icon'          => 'la-wpb-icon la_portfolio_info',
        'category'  	=> __('La Studio', 'la-studio'),
        'description' 	=> __('Display portfolio information with la-studio themes style.','la-studio'),
        'params' 		=> $shortcode_params
    ),
    'la_portfolio_info'
);