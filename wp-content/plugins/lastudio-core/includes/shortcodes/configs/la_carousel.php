<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}

if ( class_exists('WPBakeryShortCodesContainer') && !class_exists( 'WPBakeryShortCode_la_carousel' ) ) {
	class WPBakeryShortCode_la_carousel extends WPBakeryShortCodesContainer{

	}
}

return apply_filters(
	'LaStudio/shortcodes/configs',
	array(
		'name'			=> __('LA Advanced Carousel', 'la-studio'),
		'base'			=> 'la_carousel',
		'icon'          => 'la-wpb-icon la_carousel',
		'category'  	=> __('La Studio', 'la-studio'),
		'description' 	=> __('Carousel anything.','la-studio'),
		'as_parent'     => array( 'except' => array(
			'la_carousel', 'la_animation_block'
		) ),
		'content_element'=> true,
		'controls'       => 'full',
		'show_settings_on_create' => true,
		'params' 		=> LaStudio_Shortcodes_Helper::paramCarouselShortCode(),
		'js_view' 		=> 'VcColumnView',
		'html_template' => LaStudio_Plugin::$plugin_dir_path . 'includes/shortcodes/templates/la_carousel.php'
	),
    'la_carousel'
);