<?php

$slider_type = $item_space = $css_ad_carousel = $item_animation = $el_class = '';

extract( shortcode_atts( array(
    "slides_column"      => "",
    "slider_type"        => "horizontal",
    "slide_to_scroll"    => "all",
    "speed"              => "300",
    "infinite_loop"      => "",
    "autoplay"           => "",
    "autoplay_speed"     => "5000",
    "lazyload"           => "",
    "arrows"             => "",
    "dots"               => "",
    "dots_icon"          => "dlicon-dot7",
    "next_icon"          => "dlicon-arrow-right1",
    "prev_icon"          => "dlicon-arrow-left1",
    "dots_color"         => "",
    "arrow_color"        => "",
    "arrow_size"         => "20",
    "arrow_style"        => "default",
    "arrow_bg_color"     => "",
    "arrow_border_color" => "",
    "border_size"        => "1.5",
    "draggable"          => "",
    "touch_move"         => "",
    "rtl"                => "",
    "item_space"         => "15",
    "el_class"           => "",
    "item_animation"     => "",
    "adaptive_height"    => "",
    "autowidth"          => "",
    "css_ad_carousel"    => "",
    "pauseohover" 		 => "",
    "centermode" 		 => "",
    "custom_nav"         => ""
), $atts ) );


$uid = uniqid( rand() );

$design_style = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css_ad_carousel, ' ' ), 'la_carousel', $atts );

$el_class = LaStudio_Shortcodes_Helper::getExtraClass($el_class);

$wrap_data = LaStudio_Shortcodes_Helper::getParamCarouselShortCode($atts);

$elem_css = 'la-carousel-wrapper la_carousel_' . $slider_type . $design_style . $el_class;
?>
<div id="la_carousel_<?php echo esc_attr($uid)?>" class="<?php echo esc_attr($elem_css) ?>" data-gutter="<?php echo esc_attr($item_space)?>">
    <div data-la_component="AutoCarousel" class="js-el la-slick-slider" <?php echo $wrap_data ?>>
        <?php
        la_fw_override_shortcodes( $content );
        echo wpb_js_remove_wpautop( $content );
        la_fw_restore_shortcodes();
        ?>
    </div>
</div>
<span data-la_component="InsertCustomCSS" class="js-el hidden">
    #la_carousel_<?php echo esc_attr($uid)?>{
        margin-left: -<?php echo absint($item_space); ?>px;
        margin-right: -<?php echo absint($item_space); ?>px;
    }
    #la_carousel_<?php echo esc_attr($uid)?> .la-item-wrap.slick-slide{
        padding-left: <?php echo absint($item_space); ?>px;
        padding-right: <?php echo absint($item_space); ?>px;
    }
</span>