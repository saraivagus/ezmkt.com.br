<?php

$number = $orderby = $order = $hide_empty = $ids = $columns = $el_class = $output = '';

$style = $enable_custom_image_size = $img_size = $enable_carousel = $item_space = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );

extract($atts);

$css_class = 'woocommerce' . $this->getExtraClass($el_class);

if(!empty($ids)){
    $ids = explode( ',', $ids );
    $ids = array_map( 'trim', $ids );
}

$hide_empty = ( $hide_empty == 1 || $hide_empty == true) ? 1 : 0;

$number = absint($number);

// get terms and workaround WP bug with parents/pad counts
$args = array(
    'number'     => $number,
    'taxonomy'   => 'product_cat',
    'orderby'    => $orderby,
    'order'      => $order,
    'hide_empty' => $hide_empty,
    'include'    => $ids,
    'pad_counts' => true
);

if(!empty($ids) && empty($orderby)){
    $args['orderby'] = 'include';
}

$product_categories = get_terms( $args );


if ( $hide_empty ) {
    foreach ( $product_categories as $key => $category ) {
        if ( $category->count == 0 ) {
            unset( $product_categories[ $key ] );
        }
    }
}

if ( $number > 0 ) {
    $product_categories = array_slice( $product_categories, 0, $number );
}

$globalVar      = apply_filters('LaStudio/global_loop_variable', 'lastudio_loop');
$globalVarTmp   = (isset($GLOBALS[$globalVar]) ? $GLOBALS[$globalVar] : '');
$globalParams   = array();

$columns        = LaStudio_Shortcodes_Helper::getColumnFromShortcodeAtts($columns);
$loopCssClass = array();
$loopCssClass[] = 'products';
$loopCssClass[] = 'grid-items';
$loopCssClass[] = 'catalog-grid-' . $style;
$loopCssClass[] = 'grid-space-' . $item_space;
$carousel_configs = false;
if($enable_carousel == 'yes'){

    $carousel_configs = ' data-la_component="AutoCarousel" ';
    $carousel_configs .= LaStudio_Shortcodes_Helper::getParamCarouselShortCode($atts);
    $loopCssClass[] = 'js-el la-slick-slider';
}

if($enable_custom_image_size == 'yes' && !empty($img_size)){
    $globalParams['image_size'] = LaStudio_Shortcodes_Helper::getImageSizeFormString($img_size);
}

foreach( $columns as $screen => $value ){
    $loopCssClass[]  =  sprintf('%s-grid-%s-items', $screen, $value);
}

$GLOBALS[$globalVar] = $globalParams;

ob_start();

if ( $product_categories ) {

    printf(
        '<div class="product-categories-wrapper"><ul class="%s"%s>',
        esc_attr(implode(' ', $loopCssClass)),
        $carousel_configs ? $carousel_configs : ''
    );
    foreach ( $product_categories as $category ) {
        wc_get_template( 'content-product_cat.php', array(
            'category' => $category
        ) );
    }
    printf('</ul></div>');
}

woocommerce_reset_loop();
$GLOBALS[$globalVar] = $globalVarTmp;
echo '<div class="'. esc_attr($css_class) .'">' . ob_get_clean() . '</div>';