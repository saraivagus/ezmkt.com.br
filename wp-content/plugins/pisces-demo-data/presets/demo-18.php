<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_demo_18(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'header_layout',
            'value' => '2'
        ),
        array(
            'key' => 'header_full_width',
            'value' => 'yes'
        ),
        array(
            'key' => 'header_transparency',
            'value' => 'yes'
        ),
        array(
            'key' => 'enable_header_top',
            'value' => 'hide'
        ),


        array(
            'key' => 'header_height',
            'value' => '120px'
        ),
        array(
            'key' => 'header_sticky_height',
            'value' => '120px'
        ),

        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/footer_column_4',
            'value' => 'demo-08-footer-column-4'
        ),
        /**
         * Colors
         */


        array(
            'key' => 'la_custom_css',
            'value' => '
                @media (min-width: 1024px) {
                    .footer-column.footer-column-2 {
                        width: 16.66667%;
                    }
                    .footer-column.footer-column-4 {
                        width: 33.33333%;
                    }
                }
            '
        )
    );
}