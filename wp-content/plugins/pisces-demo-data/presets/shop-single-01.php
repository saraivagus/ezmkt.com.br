<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_shop_single_01(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'woocommerce_product_page_design',
            'value' => '1'
        ),


        /**
         * Filters
         */



        /**
         * Colors
         */

        array(
            'key' => 'la_custom_css',
            'value' => '

            '
        ),

    );
}