<?php
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}

function la_pisces_preset_shop_03_columns(){
    return array(

        /**
         * Settings
         */

        array(
            'key' => 'layout_archive_product',
            'value' => 'col-1c'
        ),

        array(
            'key' => 'main_full_width',
            'value' => 'no'
        ),

        array(
            'key'   => 'woocommerce_shop_page_columns',
            'value' => array(
                'xlg'   => '3',
                'lg'    => '3',
                'md'    => '2',
                'sm'    => '2',
                'xs'    => '1',
                'mb'    => '1'
            )
        ),


        array(
            'key' => 'enable_page_title_subtext',
            'value' => 'yes'
        ),

        array(
            'key' => 'page_title_custom_subtext',
            'value' => 'Nullam varius porttitor augue id rutrum duis vehicula'
        ),
        /**
         * Filters
         */

        array(
            'filter_name' => 'pisces/filter/page_title',
            'value' => '<header><h3 class="page-title">Shop 03 Columns</h3></header>'
        ),


        /**
         * Colors
         */
    );
}